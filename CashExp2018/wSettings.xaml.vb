﻿Public Class wSettings
    Private Sub wSettings_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Try

            Me.MinHeight = Me.ActualHeight
            Me.MaxHeight = Me.ActualHeight

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportErrorGlobal(oErrItem)

        End Try

    End Sub
End Class
