﻿Imports System.ComponentModel
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Threading
Imports System.Windows.Threading
Imports CashExp2018

Public Class wLoadDataFile
    Public WithEvents oLoadMgr As clsLoadManager
    Public sStartingPath As String = ""
    Public sRegexFileMatch As String = ""
    Public eFileType As clsFileLoadByType.eLoadTypes
    Public bHasFiles As Boolean = False
    Public aSourceFileList As New ArrayList


    Public Sub ShowWithSettings(StartingSourcePath As String, RegexMatchString As String, FileType As clsFileLoadByType.eLoadTypes)
        Dim sReturn As String = ""
        Try
            sStartingPath = StartingSourcePath
            sRegexFileMatch = RegexMatchString
            eFileType = FileType
            txtSourceFolder.Text = sStartingPath
            Me.ShowDialog()

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportErrorGlobal(oErrItem)

        End Try
    End Sub



    Private Sub wFileProcessing_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Try

            Me.MinHeight = Me.ActualHeight
            Me.MaxHeight = Me.ActualHeight

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportErrorGlobal(oErrItem)

        End Try

    End Sub


    Private Sub wFileProcessing_Initialized(sender As Object, e As EventArgs) Handles Me.Initialized
        Dim oFieldInfo As Reflection.FieldInfo, oDesc As DescriptionAttribute
        Dim sType As String
        Try

            goCurrLoadManager = oLoadMgr

            txtSourceFolder.Text = sStartingPath

            oFieldInfo = GetType(clsFileLoadByType.eLoadTypes).GetField(eFileType.ToString)
            oDesc = DirectCast(Attribute.GetCustomAttribute(oFieldInfo, GetType(DescriptionAttribute)), DescriptionAttribute)
            sType = oDesc.Description
            lblFileDetails.Content = "Files for " & sType

            oLoadMgr = New clsLoadManager()
            oLoadMgr.eFileType = eFileType
            oLoadMgr.sRegexFileNameMatch = sRegexFileMatch
            oLoadMgr.sFileSourceFolder = sStartingPath

            CheckControls()

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportErrorGlobal(oErrItem)

        End Try
    End Sub


    Private Sub txtSourceFolder_TextChanged(sender As Object, e As TextChangedEventArgs) Handles txtSourceFolder.TextChanged
        Try

            If Not oLoadMgr.ProcessIsActive Then
                oLoadMgr.sFileSourceFolder = txtSourceFolder.Text
                oLoadMgr.sRegexFileNameMatch = sRegexFileMatch
                GetFilesToList()
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub

    Private Sub cmdBrowse_Click(sender As Object, e As RoutedEventArgs) Handles cmdBrowse.Click
        Dim oDLG As New System.Windows.Forms.FolderBrowserDialog
        Dim sTempRegex As String, sFolderPath As String, sTemp As String
        Dim oDirForParentInfo As DirectoryInfo = Nothing
        Dim oDlgResult As Forms.DialogResult

        Try
            cmdBrowse.IsEnabled = False
            If Not oLoadMgr.ProcessIsActive Then

                oDLG.ShowNewFolderButton = False
                oDLG.Description = "Select folder to use the files as a data source"

                sFolderPath = txtSourceFolder.Text
                If sFolderPath = "" Then
                    'oDLG.RootFolder = Environment.ExpandEnvironmentVariables(GetMySettingString("ProjectDefaultPath"))
                    oDLG.SelectedPath = My.Computer.FileSystem.CurrentDirectory
                Else
                    sTempRegex = GetMySettingString("DirectoryRegex")
                    If Regex.IsMatch(sFolderPath, sTempRegex) Then
                        oDirForParentInfo = New DirectoryInfo(sFolderPath)
                        oDLG.SelectedPath = sFolderPath
                    End If

                    'If oDirForParentInfo IsNot Nothing Then oDLG.RootFolder = oDirForParentInfo.FullName
                End If

                oDlgResult = oDLG.ShowDialog()
                If oDlgResult = Forms.DialogResult.OK Then
                    txtSourceFolder.Text = oDLG.SelectedPath
                End If


            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
        Finally
            cmdBrowse.IsEnabled = True

        End Try

    End Sub

    Private Sub cmdProcessFile_Click(sender As Object, e As RoutedEventArgs) Handles cmdProcessFile.Click
        Try

            cmdProcessFile.IsEnabled = False
            oLoadMgr.Abort = False
            oLoadMgr.ActivateAllProcesses()
            CheckControls()

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub cmdStop_Click(sender As Object, e As RoutedEventArgs) Handles cmdStop.Click
        Try
            cmdStop.IsEnabled = False
            oLoadMgr.Abort = True

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub cmdClose_Click(sender As Object, e As RoutedEventArgs) Handles cmdClose.Click
        Try
            oLoadMgr.Abort = True

            Do While oLoadMgr.ProcessIsActive
                Thread.Sleep(10)
            Loop
            Me.Close()

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub wFileProcessing_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Try

            If Not oLoadMgr.ProcessIsActive Then ClosingDown()

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    ' ======================================================
    ' Put all supporting subroutines below this
    ' ======================================================



    Private Sub GetFilesToList()
        Dim sFile As String, sFileName As String
        Try
            bHasFiles = False
            lstMatchingFiles.Items.Clear()
            oLoadMgr.sFileSourceFolder = txtSourceFolder.Text
            oLoadMgr.sRegexFileNameMatch = sRegexFileMatch
            aSourceFileList = oLoadMgr.GetMatchingFiles
            For Each sFile In aSourceFileList
                sFileName = IO.Path.GetFileName(sFile)
                lstMatchingFiles.Items.Add(sFileName)
                bHasFiles = True
            Next
            CheckControls()
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub


    Private Sub AbortProcess()
        Try

            oLoadMgr.Abort = True
            Do While oLoadMgr.ProcessIsActive
                Thread.Sleep(10)
            Loop

            CheckControls()

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub CheckControls()
        Dim bEnabled As Boolean = True
        Dim bHasFiles As Boolean = False
        Dim bInporcess As Boolean = False

        Try

            txtSourceFolder.IsEnabled = False
            cmdBrowse.IsEnabled = False
            cmdProcessFile.IsEnabled = False
            cmdStop.IsEnabled = False
            cmdClose.IsEnabled = False
            cmdStop.Visibility = Visibility.Hidden
            cmdProcessFile.Visibility = Visibility.Hidden
            cmdClose.Visibility = Visibility.Hidden
            bHasFiles = aSourceFileList.Count > 0

            If oLoadMgr.ProcessIsActive Then
                If Not oLoadMgr.Abort Then
                    cmdStop.IsEnabled = True
                    cmdStop.Visibility = Visibility.Visible
                End If

            Else
                txtSourceFolder.IsEnabled = True
                cmdBrowse.IsEnabled = True
                If bHasFiles Then cmdProcessFile.IsEnabled = True
                cmdClose.IsEnabled = True
                cmdProcessFile.Visibility = Visibility.Visible
                cmdClose.Visibility = Visibility.Visible

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub


    Private Sub ClosingDown()
        Try
            oLoadMgr.Abort = True

            Do While oLoadMgr.ProcessIsActive
                Thread.Sleep(10)
            Loop
            Me.Hide()

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub


    ' ======================================================
    ' Put all class object based events below this
    ' ======================================================

    Private Sub oLoadMgr_onDSFileOpenedForReading(oSource As clsFileLoadByType) Handles oLoadMgr.onDSFileOpenedForReading
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileOpenedForReading(oSource))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try


    End Sub

    Private Sub oLoadMgr_onDSFileReadingRecord(oSource As clsFileLoadByType, RecordNo As Integer, NoOfRecords As Integer) Handles oLoadMgr.onDSFileReadingRecord
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileReadingRecord(oSource, RecordNo, NoOfRecords))
                Dispatcher.BeginInvoke(oAction)
            Else

                sbProgressLoad.Maximum = CDbl(NoOfRecords)
                sbProgressLoad.Value = CDbl(Math.Min(RecordNo, NoOfRecords))

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSFileRecordRead(oSource As clsFileLoadByType, oRec As clsOneSourcePaymentRecord) Handles oLoadMgr.onDSFileRecordRead
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileRecordRead(oSource, oRec))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSFileReadingCompleted(oSource As clsFileLoadByType) Handles oLoadMgr.onDSFileReadingCompleted
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileReadingCompleted(oSource))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSFileReadingFailed(oSource As clsFileLoadByType, sMessage As String) Handles oLoadMgr.onDSFileReadingFailed
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileReadingFailed(oSource, sMessage))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSFileFailure(oSource As clsFileLoadByType, oErrorItem As ErrorItem) Handles oLoadMgr.onDSFileError
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileFailure(oSource, oErrorItem))
                Dispatcher.BeginInvoke(oAction)
            Else
                ReportErrorGlobal(oErrorItem)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSFileStopProcessing(oSource As clsFileLoadByType) Handles oLoadMgr.onDSFileStopLoading
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileStopProcessing(oSource))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub


    Private Sub oLoadMgr_onDSFileOpenedForWriting(oSource As clsFileLoadByType) Handles oLoadMgr.onDSFileOpenedForWriting
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileOpenedForWriting(oSource))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSWritingRecord(oSource As clsFileLoadByType, RecordNo As Integer, NoOfRecords As Integer) Handles oLoadMgr.onDSWritingRecord
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSWritingRecord(oSource, RecordNo, NoOfRecords))
                Dispatcher.BeginInvoke(oAction)
            Else
                sbProgressSend.Maximum = CDbl(NoOfRecords)
                sbProgressSend.Value = CDbl(Math.Min(RecordNo, NoOfRecords))

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSRecordWritten(oSource As clsFileLoadByType, oRec As clsOneSourcePaymentRecord) Handles oLoadMgr.onDSRecordWritten
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSRecordWritten(oSource, oRec))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSFileWritingCompleted(oSource As clsFileLoadByType) Handles oLoadMgr.onDSFileWritingCompleted
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileWritingCompleted(oSource))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onDSFileWritingFailed(oSource As clsFileLoadByType, sMessage As String) Handles oLoadMgr.onDSFileWritingFailed
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onDSFileWritingFailed(oSource, sMessage))
                Dispatcher.BeginInvoke(oAction)
            Else

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onManagerProcessingStarted(oSource As clsLoadManager) Handles oLoadMgr.onManagerProcessingStarted
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onManagerProcessingStarted(oSource))
                Dispatcher.BeginInvoke(oAction)
            Else
                CheckControls()
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onManagerProcessingEnded(oSource As clsLoadManager) Handles oLoadMgr.onManagerProcessingEnded
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onManagerProcessingEnded(oSource))
                Dispatcher.BeginInvoke(oAction)
            Else
                CheckControls()
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Private Sub oLoadMgr_onManagerError(oSource As clsLoadManager, oErrItem As ErrorItem) Handles oLoadMgr.onManagerError
        Dim oDispatcher As Dispatcher = Dispatcher.FromThread(Thread.CurrentThread)
        Dim oAction As Action
        Try
            If oDispatcher Is Nothing OrElse Not oDispatcher.CheckAccess() Then
                oAction = New Action(Sub() oLoadMgr_onManagerError(oSource, oErrItem))
                Dispatcher.BeginInvoke(oAction)
            Else
                CheckControls()
            End If

        Catch ex As Exception
            Dim oErrItem2 As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub
End Class
