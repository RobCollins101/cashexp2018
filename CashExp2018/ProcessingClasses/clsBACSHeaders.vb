﻿Public Class VOL1BACS
    Public Serial As String
    Public Accessable As String
    Public OwnerID As String
    Public oParent As clsFileLoadByType
    Public OrigLine As String = ""

    Public Property VOL1Line As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(80)
                sReturn = sReturn.Substring(0, 4) &
                    PadToLength(Serial, 6) &
                    PadToLength(Accessable, 1) &
                    Space(80)
                sReturn = sReturn.Substring(0, 38) &
                    PadToLength(OwnerID, 10)
                sReturn = sReturn.PadRight(IIf(OrigLine = "", 80, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn
        End Get
        Set(value As String)
            OrigLine = value
            If value.Length >= 47 Then
                value = value.PadRight(80)
                Serial = Trim(value.Substring(5, 6))
                Accessable = Trim(value.Substring(11, 1))
                OwnerID = Trim(value.Substring(38, 10))
            End If
        End Set
    End Property
    Public Sub New(sLine As String, oParentObj As clsFileLoadByType)
        oParent = oParentObj
        VOL1Line = sLine
    End Sub
End Class



Public Class HDR1BACS
    Public FileID As String
    Public SetID As String
    Public MediaSerialNo As String
    Public FileSectNo As Integer
    Public FileSeqNo As Integer
    Public GenerationNo As Integer
    Public GenVNo As Integer
    Public CreateDate As Date
    Public ExpireDate As Date
    Public Accessability As String
    Public BlockCnt As Integer
    Public SystemCode As String
    Public oParent As clsFileLoadByType
    Public OrigLine As String = ""
    Public Property HDR1Line As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(80)
                sReturn = sReturn.Substring(0, 4) & PadToLength(FileID, 17, True) &
                    PadToLength(MediaSerialNo, 6) &
                    FileSectNo.ToString("D4") &
                    FileSeqNo.ToString("D4") &
                    GenerationNo.ToString("D4") &
                    GenVNo.ToString("D2") &
                    DateToJulian(CreateDate) &
                    DateToJulian(ExpireDate) &
                    PadToLength(Accessability, 1) &
                    BlockCnt.ToString("D6") &
                    PadToLength(SystemCode, 13, True)
                sReturn = sReturn.PadRight(IIf(OrigLine = "", 80, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn
        End Get
        Set(value As String)
            OrigLine = value
            If value.Length >= 72 Then
                value = value.PadRight(80)
                FileID = Trim(value.Substring(5, 17))
                MediaSerialNo = Trim(value.Substring(22, 6))
                FileSectNo = CInt(value.Substring(28, 4))
                FileSeqNo = CInt(value.Substring(32, 4))
                GenerationNo = CInt(value.Substring(36, 4))
                GenVNo = CInt(value.Substring(40, 2))
                CreateDate = JulianToDate(value.Substring(43, 5))
                ExpireDate = JulianToDate(value.Substring(49, 5))
                Accessability = value.Substring(54, 1)
                BlockCnt = CInt(value.Substring(55, 6))
                SystemCode = Trim(value.Substring(61, 13))
            End If

        End Set
    End Property
    Public Sub New(sLine As String, oParentObj As clsFileLoadByType)
        oParent = oParentObj
        HDR1Line = sLine
    End Sub
End Class



Public Class HDR2BACS
    Public RecFormat As String
    Public BlockSize As Integer
    Public RecLen As Integer
    Public oParent As clsFileLoadByType
    Public OrigLine As String = ""

    Public Property HDR2Line As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(80)
                sReturn = sReturn.Substring(0, 4) & PadToLength(RecFormat, 1) &
                    BlockSize.ToString("D5") &
                    RecLen.ToString("D5")
                sReturn = sReturn.PadRight(IIf(OrigLine = "", 80, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn
        End Get
        Set(value As String)
            OrigLine = value
            If value.Length >= 16 Then
                value = value.PadRight(80)
                RecFormat = value.Substring(5, 1)
                BlockSize = CInt(value.Substring(6, 5))
                RecLen = CShort(value.Substring(11, 5))
            End If
        End Set
    End Property
    Public Sub New(sLine As String, oParentObj As clsFileLoadByType)
        oParent = oParentObj
        HDR2Line = sLine
    End Sub

End Class



Public Class UHL1BACS
    Public DayOfProcess As Date
    Public RcvIdentNo As String
    Public Currency As String
    Public Country As String
    Public WorkCode As String
    Public FileNo As Integer
    Public AuditID As String
    Public oParent As clsFileLoadByType
    Public OrigLine As String = ""
    Public Property UHL1Line As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(80)
                sReturn = sReturn.Substring(0, 6) &
                    DateToJulian(DayOfProcess) &
                    PadToLength(RcvIdentNo, 10) &
                    PadToLength(Currency, 2) &
                    PadToLength(Country, 6) &
                    PadToLength(WorkCode, 9) &
                    FileNo.ToString("D3") &
                    PadToLength(AuditID, 12)
                sReturn = sReturn.PadRight(IIf(OrigLine = "", 80, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn
        End Get
        Set(value As String)
            OrigLine = value
            If value.Length >= 52 Then
                value = value.PadRight(80)
                DayOfProcess = JulianToDate(value.Substring(6, 5))
                RcvIdentNo = Trim(value.Substring(11, 10))
                Currency = Trim(value.Substring(21, 2))
                Country = Trim(value.Substring(23, 6))
                WorkCode = Trim(value.Substring(29, 9))
                FileNo = CInt(value.Substring(38, 3))
                AuditID = Trim(value.Substring(41, 12))
            End If
        End Set
    End Property
    Public Sub New(sLine As String, oParentObj As clsFileLoadByType)
        oParent = oParentObj
        UHL1Line = sLine
    End Sub
End Class



Public Class EOF1BACS
    Public FileID As String
    Public SetID As String
    Public MediaSerialNo As String
    Public FileSectNo As Integer
    Public FileSeqNo As Integer
    Public GenerationNo As Integer
    Public GenVNo As Integer
    Public CreateDate As Date
    Public ExpireDate As Date
    Public Accessability As String
    Public BlockCnt As Integer
    Public SystemCode As String
    Public oParent As clsFileLoadByType
    Public OrigLine As String = ""
    Public Property EOF1Line As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(80)
                sReturn = sReturn.Substring(0, 4) &
                    PadToLength(FileID, 1) &
                    PadToLength(MediaSerialNo, 6, True) &
                    FileSectNo.ToString("D4") &
                    FileSeqNo.ToString("D4") &
                    GenerationNo.ToString("D4") &
                    GenVNo.ToString("D2") &
                    DateToJulian(CreateDate) &
                    DateToJulian(ExpireDate) &
                    BlockCnt.ToString("D6") &
                    PadToLength(SystemCode, 13)
                sReturn = sReturn.PadRight(IIf(OrigLine = "", 80, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn
        End Get
        Set(value As String)
            OrigLine = value
            If value.Length >= 73 Then
                value = value.PadRight(80)
                FileID = value.Substring(5, 17)
                MediaSerialNo = value.Substring(22, 6)
                FileSectNo = CInt(value.Substring(28, 4))
                FileSeqNo = CInt(value.Substring(32, 4))
                GenerationNo = CInt(value.Substring(36, 4))
                GenVNo = CInt(value.Substring(40, 2))
                CreateDate = JulianToDate(value.Substring(43, 5))
                ExpireDate = JulianToDate(value.Substring(49, 5))
                Accessability = value.Substring(54, 1)
                BlockCnt = CInt(value.Substring(55, 6))
                SystemCode = value.Substring(61, 13)
            End If

        End Set
    End Property
    Public Sub New(sLine As String, oParentObj As clsFileLoadByType)
        oParent = oParentObj
        EOF1Line = sLine
    End Sub
End Class



Public Class EOF2BACS
    Public RecFormat As String
    Public BlockSize As Integer
    Public RecLen As Integer
    Public oParent As clsFileLoadByType
    Public OrigLine As String = ""
    Public Property EOF2Line As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(80)
                sReturn = sReturn.Substring(0, 4) &
                    PadToLength(RecFormat, 1) &
                    BlockSize.ToString("D5") &
                    RecLen.ToString("D5")
                sReturn = sReturn.PadRight(IIf(OrigLine = "", 80, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn
        End Get
        Set(value As String)
            OrigLine = value
            If value.Length >= 16 Then
                value = value.PadRight(80)
                RecFormat = value.Substring(5, 1)
                BlockSize = CInt(value.Substring(6, 5))
                RecLen = CInt(value.Substring(11, 5))
            End If
        End Set
    End Property
    Public Sub New(sLine As String, oParentObj As clsFileLoadByType)
        oParent = oParentObj
        EOF2Line = sLine
    End Sub

End Class



Public Class UTL1BACS
    Public DebitAmt As Decimal
    Public CreditAmt As Decimal
    Public DebitCnt As Integer
    Public CreditCnt As Integer
    Public oParent As clsFileLoadByType
    Public OrigLine As String = ""
    Public Property UTL1Line As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(80)
                sReturn = sReturn.Substring(0, 4) &
                    PadToLength(DebitAmt.ToString("F2"), 13, True) &
                    PadToLength(DebitAmt.ToString("F2"), 13, True) &
                    DebitAmt.ToString("D7") &
                    DebitAmt.ToString("D7")
                sReturn = sReturn.PadRight(IIf(OrigLine = "", 80, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn
        End Get
        Set(value As String)
            OrigLine = value
            If value.Length >= 45 Then
                value = value.PadRight(80)
                DebitAmt = CDec(value.Substring(5, 13))
                CreditAmt = CDec(value.Substring(18, 13))
                DebitCnt = CShort(value.Substring(31, 7))
                CreditCnt = CShort(value.Substring(38, 7))
            End If

        End Set
    End Property
    Public Sub New(sLine As String, oParentObj As clsFileLoadByType)
        oParent = oParentObj
        UTL1Line = sLine
    End Sub
End Class
