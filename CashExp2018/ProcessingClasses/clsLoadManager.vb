﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Threading
Imports System.Windows.Threading
Imports CashExp2018

Public Class clsLoadManager

    Public Event onDSFileOpenedForReading(oSource As clsFileLoadByType)
    Public Event onDSFileReadingFailed(oSource As clsFileLoadByType, sMessage As String)
    Public Event onDSFileReadingRecord(oSource As clsFileLoadByType, RecordNo As Integer, NoOfRecords As Integer)
    Public Event onDSFileRecordRead(oSource As clsFileLoadByType, oRec As clsOneSourcePaymentRecord)
    Public Event onDSFileReadingCompleted(oSource As clsFileLoadByType)

    Public Event onDSFileError(oSource As clsFileLoadByType, oErrItem As ErrorItem)
    Public Event onDSFileStopLoading(oSource As clsFileLoadByType)


    Public Event onDSFileOpenedForWriting(oSource As clsFileLoadByType)
    Public Event onDSFileWritingFailed(oSource As clsFileLoadByType, sMessage As String)
    Public Event onDSWritingRecord(oSource As clsFileLoadByType, RecordNo As Integer, NoOfRecords As Integer)
    Public Event onDSRecordWritten(oSource As clsFileLoadByType, oRec As clsOneSourcePaymentRecord)
    Public Event onDSFileWritingCompleted(oSource As clsFileLoadByType)

    Public Event onDSStopWriting(oSource As clsFileLoadByType)

    Public Event onManagerProcessingStarted(oSource As clsLoadManager)
    Public Event onManagerProcessingEnded(oSource As clsLoadManager)
    Public Event onManagerError(oSource As clsLoadManager, oErrItem As ErrorItem)

    Public WithEvents oCurrSourceFile As clsFileLoadByType

    Public sFileSourceFolder As String = ""
    Public sFileDestPath As String = ""
    Public sRegexFileNameMatch As String = ""
    Public sFileBackupTemplate As String = ""
    Public eFileType As clsFileLoadByType.eLoadTypes
    Public sCurrFile As String

    Public oTaskAction As Task = Nothing
    Public oAction As Action
    Public bAbort As Boolean = False
    Public bEnding As Boolean = False


    Public Property Abort As Boolean
        Get
            Return bAbort
        End Get
        Set(value As Boolean)
            bAbort = value
            If bAbort And ProcessIsActive() Then
                oCurrSourceFile.Abort = bAbort

            End If
        End Set
    End Property


    Public Function ProcessIsActive() As Boolean
        Dim bReturn As Boolean = False
        Try

            If oTaskAction IsNot Nothing Then
                bReturn = (oTaskAction.Status = TaskStatus.Running Or
                    oTaskAction.Status = TaskStatus.WaitingForActivation Or
                    oTaskAction.Status = TaskStatus.WaitingForChildrenToComplete Or
                    oTaskAction.Status = TaskStatus.WaitingToRun) And
                    Not bEnding
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return bReturn
    End Function


    Public Async Sub ActivateAllProcesses()
        Dim oTask As Task(Of System.Data.DataTable) = Nothing

        Try

            bEnding = False
            RaiseEvent onManagerProcessingStarted(Me)

            If Not bAbort Then
                oAction = New Action(AddressOf DoAllProcesses)
                oTaskAction = Task.Run(oAction)
                oTaskAction.Yield()
            End If

        Catch ex As Exception
            bEnding = True
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            RaiseEvent onManagerError(Me, oErrItem)
            RaiseEvent onManagerProcessingEnded(Me)

        End Try

    End Sub


    Public Function GetMatchingFiles() As ArrayList
        Dim aReturn As New ArrayList, aFiles() As FileInfo
        Dim sFilename As String, oFileInfo As FileInfo, oDir As DirectoryInfo
        Try
            If IO.Directory.Exists(sFileSourceFolder) Then
                oDir = New DirectoryInfo(sFileSourceFolder)
                aFiles = oDir.GetFiles
                'aFiles = IO.Directory.GetFiles(sFileSourceFolder)
                For Each oFileInfo In aFiles
                    If bAbort Then Exit For
                    sFilename = oFileInfo.Name
                    'sFilename = IO.Path.GetFileName(sFilePath)
                    If Regex.IsMatch(sFilename, sRegexFileNameMatch) Then aReturn.Add(oFileInfo.FullName)
                Next
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            bEnding = True
            RaiseEvent onManagerError(Me, oErrItem)

        End Try
        Return aReturn
    End Function


    Public Sub DoAllProcesses()
        Dim aFiles As ArrayList, sFilePath As String, sFilename As String
        Dim aFilesToProcess As New ArrayList

        Try
            bEnding = False
            aFilesToProcess = GetMatchingFiles()

            If Not bAbort Then
                For Each sFilePath In aFilesToProcess
                    ProcessOneFile(sFilePath)
                Next
            End If

            If bAbort Then RaiseEvent onManagerError(Me, New ErrorItem("Process aborted"))
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            bEnding = True
            RaiseEvent onManagerError(Me, oErrItem)

        Finally
            bEnding = True
            RaiseEvent onManagerProcessingEnded(Me)

        End Try

    End Sub

    Public Function ProcessOneFile(sSourceFilePath As String) As Boolean
        Dim bReturn As Boolean = False
        Try
            oCurrSourceFile = New clsFileLoadByType(sSourceFilePath, sFileDestPath, Me)
            oCurrSourceFile.BackupFilePathTemplate = sFileBackupTemplate
            If Not bAbort Then bReturn = ProcessOneFileLoading()
            If Not bAbort And bReturn And oCurrSourceFile.Count > 0 Then bReturn = ProcessOneFileWriting()
            bEnding = True

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            bEnding = True
            RaiseEvent onManagerError(Me, oErrItem)

        Finally
            bEnding = True
            RaiseEvent onManagerProcessingEnded(Me)
        End Try

        Return bReturn
    End Function

    Public Function ProcessOneFileLoading() As Boolean
        Dim bReturn As Boolean = False
        If Not bAbort Then bReturn = oCurrSourceFile.LoadFileData()

        Return bReturn
    End Function

    Public Function ProcessOneFileWriting() As Boolean
        Dim bReturn As Boolean = False

        If Not bAbort Then bReturn = oCurrSourceFile.BackupFile()
        If bReturn Then bReturn = oCurrSourceFile.SaveLoadedData()
        If bReturn Then bReturn = oCurrSourceFile.RemoveSourceFile(True)

        Return bReturn
    End Function


    ' ===============================================
    ' Event handling and passing processes
    ' ===============================================

    Private Sub oSourceFile_onFileOpenedForReading(oSource As clsFileLoadByType) Handles oCurrSourceFile.onFileOpenedForReading

        RaiseEvent onDSFileOpenedForReading(oSource)

    End Sub

    Private Sub oSourceFile_onReadingRecord(oSource As clsFileLoadByType, RecordNo As Integer, NoOfRecords As Integer) Handles oCurrSourceFile.onReadingRecord

        RaiseEvent onDSFileReadingRecord(oSource, RecordNo, NoOfRecords)

    End Sub

    Private Sub oSourceFile_onRecordRead(oSource As clsFileLoadByType, oRec As clsOneSourcePaymentRecord) Handles oCurrSourceFile.onRecordRead

        RaiseEvent onDSFileRecordRead(oSource, oRec)

    End Sub

    Private Sub oSourceFile_onFileReadingCompleted(oSource As clsFileLoadByType) Handles oCurrSourceFile.onFileReadingCompleted

        RaiseEvent onDSFileReadingCompleted(oSource)

    End Sub

    Private Sub oSourceFile_onStopLoading(oSource As clsFileLoadByType) Handles oCurrSourceFile.onStopLoading

        RaiseEvent onDSFileStopLoading(oSource)

    End Sub

    Private Sub oSourceFile_onFileReadingFailed(oSource As clsFileLoadByType, sMessage As String) Handles oCurrSourceFile.onFileReadingFailed

        RaiseEvent onDSFileReadingFailed(oSource, sMessage)

    End Sub

    Private Sub oSourceFile_onFailure(oSource As clsFileLoadByType, oErrItem As ErrorItem) Handles oCurrSourceFile.onError

        RaiseEvent onDSFileError(oSource, oErrItem)

    End Sub

    Private Sub oSourceFile_onFileOpenedForWriting(oSource As clsFileLoadByType) Handles oCurrSourceFile.onFileOpenedForWriting

        RaiseEvent onDSFileOpenedForWriting(oSource)

    End Sub

    Private Sub oSourceFile_onWritingRecord(oSource As clsFileLoadByType, RecordNo As Integer, NoOfRecords As Integer) Handles oCurrSourceFile.onWritingRecord

        RaiseEvent onDSWritingRecord(oSource, RecordNo, NoOfRecords)

    End Sub

    Private Sub oSourceFile_onRecordWritten(oSource As clsFileLoadByType, oRec As clsOneSourcePaymentRecord) Handles oCurrSourceFile.onRecordWritten

        RaiseEvent onDSRecordWritten(oSource, oRec)

    End Sub

    Private Sub oSourceFile_onFileWritingCompleted(oSource As clsFileLoadByType) Handles oCurrSourceFile.onFileWritingCompleted

        RaiseEvent onDSFileWritingCompleted(oSource)

    End Sub

    Private Sub oSourceFile_onStopWriting(oSource As clsFileLoadByType) Handles oCurrSourceFile.onStopWriting

        RaiseEvent onDSStopWriting(oSource)

    End Sub

    Private Sub oSourceFile_onFileWritingFailed(oSource As clsFileLoadByType, sMessage As String) Handles oCurrSourceFile.onFileWritingFailed

        RaiseEvent onDSFileWritingFailed(oSource, sMessage)

    End Sub

    ' ===============================================
    ' Functions for specific requirements
    ' ===============================================

    Protected Friend Function DetailsAsSortedList() As SortedList(Of String, String)
        Dim aReturn As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
        Dim sVal As String, sVal2 As String
        Try

            aReturn = oCurrSourceFile.DetailsAsSortedList

            TryColAdd(aReturn, "FileSourceFolder", sFileSourceFolder)
            TryColAdd(aReturn, "SourceFolder", sFileSourceFolder)
            TryColAdd(aReturn, "LoadedFolder", sFileSourceFolder)
            TryColAdd(aReturn, "UploadFolder", sFileSourceFolder)

            TryColAdd(aReturn, "SourceFile", sCurrFile)
            TryColAdd(aReturn, "SourceCashFile", sCurrFile)
            TryColAdd(aReturn, "Source", sCurrFile)
            TryColAdd(aReturn, "CurrSourceFile", sCurrFile)
            TryColAdd(aReturn, "CurrSourceCashFile", sCurrFile)
            TryColAdd(aReturn, "CurrSource", sCurrFile)

            TryColAdd(aReturn, "DestinationFile", sFileDestPath)
            TryColAdd(aReturn, "DestFile", sFileDestPath)
            TryColAdd(aReturn, "CashFile", sFileDestPath)
            TryColAdd(aReturn, "OutputFile", sFileDestPath)
            TryColAdd(aReturn, "Type", eFileType.ToString)



        Catch ex As Exception

            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            bEnding = True
            RaiseEvent onManagerError(Me, oErrItem)
        End Try
        Return aReturn
    End Function

End Class
