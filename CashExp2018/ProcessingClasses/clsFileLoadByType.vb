﻿Imports System.ComponentModel
Imports System.IO
Imports System.Text.RegularExpressions
Imports CashExp2018

''' <summary>
''' Base class for a file's loaded details. Based on BACS format
''' </summary>
Public Class clsFileLoadByType
    Inherits List(Of clsOneSourcePaymentRecord)

    Public Enum eLoadTypes
        <Description("BACS transfer file")> BACS
        <Description("Unpaid Direct Debits")> UDD
        <Description("Giro payments")> Giro
        <Description("Head Office Collection Account file")> HOCA
        '<Description("Santander transfer file")> Santander
        <Description("Fastpay Payments")> Fastpay
    End Enum


    Public Enum SortPayments
        ByFundReference
        ByPostDate
        BySource
    End Enum
    Public Property Vol1Rec As VOL1BACS
    Public Property HDR1Rec As HDR1BACS
    Public Property HDR2Rec As HDR2BACS
    Public Property UHL1Rec As UHL1BACS
    Public Property EOF1Rec As EOF1BACS
    Public Property EOF2Rec As EOF2BACS
    Public Property UTL1Rec As UTL1BACS

    Friend bLowestFirstSortOrder As Boolean = True
    Friend eSortBy As SortPayments = SortPayments.ByFundReference

    Public bAbort As Boolean = False

    Public Event onFileOpenedForReading(oSource As clsFileLoadByType)
    Public Event onFileReadingFailed(oSource As clsFileLoadByType, sMessage As String)
    Public Event onReadingRecord(oSource As clsFileLoadByType, RecordNo As Integer, NoOfRecords As Integer)
    Public Event onRecordRead(oSource As clsFileLoadByType, oRec As clsOneSourcePaymentRecord)
    Public Event onFileReadingCompleted(oSource As clsFileLoadByType)
    Public Event onFileReadingAborted(oSource As clsFileLoadByType)
    Public Event onError(oSource As clsFileLoadByType, oErrItem As ErrorItem)
    Public Event onStopLoading(oSource As clsFileLoadByType)

    Public Event onFileOpenedForWriting(oSource As clsFileLoadByType)
    Public Event onFileWritingFailed(oSource As clsFileLoadByType, sMessage As String)
    Public Event onWritingRecord(oSource As clsFileLoadByType, RecordNo As Integer, NoOfRecords As Integer)
    Public Event onRecordWritten(oSource As clsFileLoadByType, oRec As clsOneSourcePaymentRecord)
    Public Event onFileWritingCompleted(oSource As clsFileLoadByType)
    Public Event onFileWritingAborted(oSource As clsFileLoadByType)

    Public Event onStopWriting(oSource As clsFileLoadByType)

    Private iNoOfSourceDataRecords As Integer = -1, iRecordNo As Integer = -1
    Public oLoadManager As clsLoadManager

    Private oSourceDataFileInfo As FileInfo
    Private sBackupFilePathTemplate As String = ""
    Private oBackupDataFileInfo As FileInfo
    Private oDestDataFileInfo As FileInfo

    Public oCurrRecord As clsOneSourcePaymentRecord

    Public Sub New()

    End Sub

    Public Sub New(sSourceFilePath As String, sDestFilePath As String, clsLoadManager As clsLoadManager)
        Try
            SourceFilePath = sSourceFilePath
            DestFilePath = sDestFilePath
            oLoadManager = clsLoadManager
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            RaiseEvent onError(Me, oErrItem)

        End Try
    End Sub

    Public Function NoOfSourceDataRecords() As Integer
        Dim oTextReader As TextReader = Nothing, sLine As String
        Dim sType As String, bTransLine As Boolean
        Try
            If iNoOfSourceDataRecords >= 0 Or oSourceDataFileInfo Is Nothing OrElse Not oSourceDataFileInfo.Exists Then
            Else
                oTextReader = oSourceDataFileInfo.OpenText
                iNoOfSourceDataRecords = 0
                Do While oTextReader.Peek >= 0 And Not bAbort
                    sLine = oTextReader.ReadLine()
                    bTransLine = False
                    If sLine.Length > 4 Then
                        sType = sLine.Substring(0, 4)
                        Select Case sType
                            Case "VOL1", "HDR1", "HDR2", "UHL1", "EOF1", "EOF2", "UTL1"
                            Case Else : bTransLine = (sLine.Length > 5)
                        End Select

                        If bTransLine Then iNoOfSourceDataRecords += 1
                    End If
                    If bAbort Then Exit Do
                Loop
                oTextReader.Close()
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            RaiseEvent onError(Me, oErrItem)

        End Try
        If oTextReader IsNot Nothing Then Try : oTextReader.Close() : Catch ex As Exception : End Try
        Return iNoOfSourceDataRecords
    End Function

    Property LowestFirstSortOrder As Boolean
        Get
            Return bLowestFirstSortOrder
        End Get
        Set(value As Boolean)
            bLowestFirstSortOrder = value
            Me.Sort()
        End Set
    End Property

    Property SortBy As SortPayments
        Get
            Return eSortBy
        End Get
        Set(value As SortPayments)
            eSortBy = value
            Me.Sort()
        End Set
    End Property

    Public Overloads Sub Add(PaymentObj As clsOneSourcePaymentRecord)
        PaymentObj.oParentCol = Me
        MyBase.Add(PaymentObj)
    End Sub


    Public Property SourceFilePath As String
        Get
            Dim sReturn As String = ""
            If oSourceDataFileInfo IsNot Nothing Then
                sReturn = oSourceDataFileInfo.FullName
            End If
            Return sReturn
        End Get
        Set(value As String)
            Dim bNewObj As Boolean = False
            Try
                bNewObj = (oSourceDataFileInfo Is Nothing)
                If oSourceDataFileInfo IsNot Nothing AndAlso Path.GetFullPath(value) <> oSourceDataFileInfo.FullName Then bNewObj = True
                If bNewObj Then oSourceDataFileInfo = New FileInfo(value)

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
                RaiseEvent onError(Me, oErrItem)

            End Try

        End Set

    End Property


    Public Property BackupFilePathTemplate As String
        Get
            Return sBackupFilePathTemplate
        End Get
        Set(value As String)
            If value <> sBackupFilePathTemplate Then
                sBackupFilePathTemplate = value
                oBackupDataFileInfo = Nothing
            End If
        End Set
    End Property


    Public Property BackupFilePath As String
        Get
            Dim sReturn As String = ""
            If oBackupDataFileInfo Is Nothing AndAlso sBackupFilePathTemplate <> "" Then
                sReturn = Me.SubstitutedValue(sBackupFilePathTemplate)
                Try : oBackupDataFileInfo = New FileInfo(sReturn) : Catch ex As Exception : End Try
            End If
            If oBackupDataFileInfo IsNot Nothing Then
                sReturn = oBackupDataFileInfo.FullName
            End If
            Return sReturn
        End Get
        Set(value As String)
            Dim bNewObj As Boolean = False, sTempPath As String, sTempRegex As String
            Try
                bNewObj = (oBackupDataFileInfo Is Nothing)
                sTempRegex = GetMySettingString("FilePathRegex")
                If value = "" Then
                    sTempPath = Me.SubstitutedValue(sBackupFilePathTemplate)
                    If sTempRegex = "" OrElse Regex.IsMatch(sTempPath, sTempRegex) Then
                        Try : oBackupDataFileInfo = New FileInfo(sTempPath) : Catch ex As Exception : End Try
                    End If
                Else
                    sTempPath = value
                End If

                If sTempRegex = "" OrElse Regex.IsMatch(sTempPath, sTempRegex) Then
                    If oBackupDataFileInfo IsNot Nothing AndAlso Path.GetFullPath(sTempPath) <> oBackupDataFileInfo.FullName Then bNewObj = True
                    If bNewObj Then
                        Try : oBackupDataFileInfo = New FileInfo(sTempPath) : Catch ex As Exception : End Try
                    End If
                End If

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
                RaiseEvent onError(Me, oErrItem)

            End Try

        End Set

    End Property



    Public Property DestFilePath As String
        Get
            Dim sReturn As String = ""
            If oDestDataFileInfo IsNot Nothing Then
                sReturn = oDestDataFileInfo.FullName
            End If
            Return sReturn
        End Get
        Set(value As String)
            Dim bNewObj As Boolean = False, sTemp As String
            Try
                bNewObj = (oDestDataFileInfo Is Nothing)
                If value <> "" Then
                    sTemp = GetMySettingString("FilePathRegex")
                    If sTemp = "" OrElse Regex.IsMatch(value, sTemp) Then
                        If oDestDataFileInfo IsNot Nothing AndAlso Path.GetFullPath(value) <> oDestDataFileInfo.FullName Then bNewObj = True
                        If bNewObj Then oDestDataFileInfo = New FileInfo(value)
                    End If
                End If

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
                RaiseEvent onError(Me, oErrItem)

            End Try

        End Set

    End Property

    Public Property Abort As Boolean
        Get
            Return bAbort
        End Get
        Set(value As Boolean)
            bAbort = value
        End Set
    End Property

    Public Function BackupFile() As Boolean
        Dim bReturn As Boolean = False, iBlock As Integer
        Dim oWriteStream As StreamWriter = Nothing, oReadStream As StreamReader = Nothing
        Try

            If BackupFilePath <> "" AndAlso oBackupDataFileInfo IsNot Nothing Then
                oSourceDataFileInfo.CopyTo(oBackupDataFileInfo.FullName, False)

                'oReadStream = oSourceDataFileInfo.OpenText
                'oWriteStream = New StreamWriter(oBackupDataFileInfo.OpenWrite)
                'Do While oReadStream.Peek >= 0
                '    oWriteStream.WriteLine(oReadStream.ReadLine)
                '    oWriteStream.Flush()
                'Loop
            End If
            bReturn = oBackupDataFileInfo.Exists
            If Not bReturn Then
                RaiseEvent onError(Me, New ErrorItem("Cannot back up source file """ & oSourceDataFileInfo.FullName & """ to """ & oBackupDataFileInfo.FullName & """"))
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            RaiseEvent onError(Me, oErrItem)

        Finally
            If oReadStream IsNot Nothing Then Try : oReadStream.Close() : Catch ex As Exception : End Try
            oReadStream = Nothing
            If oWriteStream IsNot Nothing Then Try : oWriteStream.Flush() : oWriteStream.Close() : Catch ex As Exception : End Try
            oWriteStream = Nothing
        End Try
        Return bReturn
    End Function

    Public Function RemoveSourceFile(bBackupNeeded As Boolean) As Boolean
        Dim bReturn As Boolean = False
        Try
            If Not bBackupNeeded Or (oBackupDataFileInfo IsNot Nothing AndAlso oBackupDataFileInfo.Exists) Then
                oSourceDataFileInfo.Delete()
                oSourceDataFileInfo = Nothing
            End If
            bReturn = True
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            RaiseEvent onError(Me, oErrItem)

        End Try
        Return bReturn
    End Function

    Public Function SaveLoadedData() As Boolean
        Dim bReturn As Boolean = False
        Dim oTextWriter As StreamWriter = Nothing, sLine As String
        Dim sType As String
        'Dim oRec As clsOneSourcePaymentRecord
        Dim iPos As Integer = 0, iMax As Integer = 0

        Try
            If Me.Count <= 0 Or oDestDataFileInfo Is Nothing Then
            Else
                iMax = Me.Count - 1
                RaiseEvent onFileOpenedForWriting(Me)
                Me.Sort()
                oTextWriter = File.AppendText(oDestDataFileInfo.FullName)
                oTextWriter.AutoFlush = True
                For Each oCurrRecord In Me
                    iPos += 1
                    RaiseEvent onWritingRecord(Me, iPos, iMax)
                    sLine = oCurrRecord.PaymentLineOut
                    oTextWriter.WriteLine(sLine)
                    RaiseEvent onRecordWritten(Me, oCurrRecord)

                    If bAbort Then Exit For

                Next
                bReturn = True
                If bAbort Then
                    RaiseEvent onFileWritingCompleted(Me)
                Else
                    RaiseEvent onFileWritingAborted(Me)
                End If

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            RaiseEvent onFileWritingFailed(Me, oErrItem.ToString)
            RaiseEvent onError(Me, oErrItem)

        Finally
            RaiseEvent onStopWriting(Me)

        End Try
        If oTextWriter IsNot Nothing Then Try : oTextWriter.Flush() : oTextWriter.Close() : Catch ex As Exception : End Try
        Return bReturn
    End Function


    Public Sub ClearData()
        Try
            Me.Clear()
            iNoOfSourceDataRecords = -1
            Vol1Rec = New VOL1BACS("", Me)
            HDR1Rec = New HDR1BACS("", Me)
            HDR2Rec = New HDR2BACS("", Me)
            UHL1Rec = New UHL1BACS("", Me)
            EOF1Rec = New EOF1BACS("", Me)
            EOF2Rec = New EOF2BACS("", Me)
            UTL1Rec = New UTL1BACS("", Me)

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Main class to load up data from the file
    ''' First thing tp do is to clear all the data related values
    ''' </summary>
    Public Function LoadFileData() As Boolean
        Dim oTextReader As TextReader = Nothing, bReturn As Boolean = False
        Dim sLine As String = "", sType As String
        Dim bTransLine As Boolean = False
        'Dim oTransDetailLine As clsOneSourcePaymentRecord

        Try

            ClearData()

            If oSourceDataFileInfo IsNot Nothing AndAlso oSourceDataFileInfo.Exists Then
                If NoOfSourceDataRecords() < 0 Then
                    RaiseEvent onFileReadingFailed(Me, "No Transactions")
                    RaiseEvent onStopLoading(Me)
                Else

                    RaiseEvent onReadingRecord(Me, 0, NoOfSourceDataRecords)
                    iRecordNo = 0
                    oTextReader = oSourceDataFileInfo.OpenText
                    Do While oTextReader.Peek >= 0 And Not bAbort
                        sLine = oTextReader.ReadLine()
                        bTransLine = False
                        If sLine.Length > 4 Then
                            sType = sLine.Substring(0, 4)
                            Select Case sType
                                Case "VOL1" : Vol1Rec = New VOL1BACS(sLine, Me)
                                Case "HDR1" : HDR1Rec = New HDR1BACS(sLine, Me)
                                Case "HDR2" : HDR2Rec = New HDR2BACS(sLine, Me)
                                Case "UHL1" : UHL1Rec = New UHL1BACS(sLine, Me)
                                Case "EOF1" : EOF1Rec = New EOF1BACS(sLine, Me)
                                Case "EOF2" : EOF2Rec = New EOF2BACS(sLine, Me)
                                Case "UTL1" : UTL1Rec = New UTL1BACS(sLine, Me)
                                Case Else : bTransLine = (sLine.Length > 5)
                            End Select

                            If bTransLine Then
                                RaiseEvent onReadingRecord(Me, 0, NoOfSourceDataRecords)
                                iRecordNo += 1
                                oCurrRecord = New clsOneSourcePaymentRecord(sLine, Me)
                                oCurrRecord.iSourceRecordNo = iRecordNo
                                RaiseEvent onRecordRead(Me, oCurrRecord)
                            End If
                        End If
                    Loop
                    bReturn = True
                    oTextReader.Close()
                    If bAbort Then
                        RaiseEvent onFileReadingAborted(Me)
                    Else
                        RaiseEvent onFileReadingCompleted(Me)
                    End If
                End If
            Else
                RaiseEvent onFileReadingFailed(Me, "File """ & oSourceDataFileInfo.FullName & """ not readable")
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            RaiseEvent onError(Me, oErrItem)
            RaiseEvent onStopLoading(Me)
            RaiseEvent onFileReadingFailed(Me, oErrItem.ToString)
        End Try
        If oTextReader IsNot Nothing Then Try : oTextReader.Close() : Catch ex As Exception : End Try
    End Function

    Protected Friend Function DetailsAsSortedList() As SortedList(Of String, String)
        Dim aReturn As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
        Dim sVal As String, sVal2 As String, sTemp As String, sTemp2 As String, iIndexOf As Integer
        Try

            If oLoadManager IsNot Nothing Then aReturn = oLoadManager.DetailsAsSortedList

            TryColAdd(aReturn, "FileSource", SourceFilePath)
            TryColAdd(aReturn, "Source", SourceFilePath)
            TryColAdd(aReturn, "SourceFile", SourceFilePath)
            TryColAdd(aReturn, "FilePathSource", SourceFilePath)
            TryColAdd(aReturn, "SourcePath", SourceFilePath)
            TryColAdd(aReturn, "SourceFilePath", SourceFilePath)

            sTemp = IO.Path.GetFileName(SourceFilePath)
            TryColAdd(aReturn, "FileNameSource", sTemp)
            TryColAdd(aReturn, "SourceFileName", sTemp)

            sTemp = IO.Path.GetFileName(SourceFilePath)
            If sTemp.LastIndexOf(".") > sTemp.Length - 5 Then

                sTemp2 = sTemp.Substring(0, sTemp.LastIndexOf("."))
                TryColAdd(aReturn, "FileNameSourceNoExt", sTemp2)
                TryColAdd(aReturn, "FileNameNoExtSource", sTemp2)
                TryColAdd(aReturn, "SourceFileNameNoExt", sTemp2)

                sTemp2 = sTemp.Substring(sTemp.LastIndexOf("."))
                TryColAdd(aReturn, "FileNameSourceExt", sTemp2)
                TryColAdd(aReturn, "FileNameExtSource", sTemp2)
                TryColAdd(aReturn, "SourceFileNameExt", sTemp2)

            End If

            TryColAdd(aReturn, "FileDest", DestFilePath)
            TryColAdd(aReturn, "Dest", DestFilePath)
            TryColAdd(aReturn, "DestFile", DestFilePath)
            TryColAdd(aReturn, "FilePathDest", DestFilePath)
            TryColAdd(aReturn, "DestPath", DestFilePath)
            TryColAdd(aReturn, "DestFilePath", DestFilePath)
            TryColAdd(aReturn, "FileDestination", DestFilePath)
            TryColAdd(aReturn, "Destination", DestFilePath)
            TryColAdd(aReturn, "DestinationFile", DestFilePath)
            TryColAdd(aReturn, "FilePathDestination", DestFilePath)
            TryColAdd(aReturn, "DestinationPath", DestFilePath)
            TryColAdd(aReturn, "DestinationFilePath", DestFilePath)
            TryColAdd(aReturn, "FileCash", DestFilePath)
            TryColAdd(aReturn, "CashFile", DestFilePath)
            TryColAdd(aReturn, "FilePathCash", DestFilePath)
            TryColAdd(aReturn, "CashPath", DestFilePath)
            TryColAdd(aReturn, "CashFilePath", DestFilePath)
            TryColAdd(aReturn, "FileTarget", DestFilePath)
            TryColAdd(aReturn, "TargetFile", DestFilePath)
            TryColAdd(aReturn, "FilePathTarget", DestFilePath)
            TryColAdd(aReturn, "TargetPath", DestFilePath)
            TryColAdd(aReturn, "TargetFilePath", DestFilePath)

            sTemp = IO.Path.GetDirectoryName(DestFilePath)
            TryColAdd(aReturn, "FolderDest", sTemp)
            TryColAdd(aReturn, "DestFolder", sTemp)
            TryColAdd(aReturn, "FolderPathDest", sTemp)
            TryColAdd(aReturn, "FolderDestination", sTemp)
            TryColAdd(aReturn, "DestinationFolder", sTemp)
            TryColAdd(aReturn, "FolderPathDestination", sTemp)
            TryColAdd(aReturn, "DestinationFolderPath", sTemp)
            TryColAdd(aReturn, "FolderCash", sTemp)
            TryColAdd(aReturn, "CashFolder", sTemp)
            TryColAdd(aReturn, "FolderPathCash", sTemp)
            TryColAdd(aReturn, "CashFolderPath", sTemp)
            TryColAdd(aReturn, "FolderTarget", sTemp)
            TryColAdd(aReturn, "TargetFolder", sTemp)
            TryColAdd(aReturn, "FolderPathTarget", sTemp)
            TryColAdd(aReturn, "TargetFolderPath", sTemp)

            sTemp = NoOfSourceDataRecords.ToString
            TryColAdd(aReturn, "NoOfRecs", sTemp)
            TryColAdd(aReturn, "NumberOfRecs", sTemp)
            TryColAdd(aReturn, "NoOfSourceRecs", sTemp)
            TryColAdd(aReturn, "NumberOfSourceRecs", sTemp)
            TryColAdd(aReturn, "CountOfRecs", sTemp)
            TryColAdd(aReturn, "CountOfSourceRecs", sTemp)
            TryColAdd(aReturn, "RecsCount", sTemp)
            TryColAdd(aReturn, "SourceRecsCount", sTemp)
            TryColAdd(aReturn, "NoOfRecords", sTemp)
            TryColAdd(aReturn, "NumberOfRecords", sTemp)
            TryColAdd(aReturn, "NoOfSourceRecords", sTemp)
            TryColAdd(aReturn, "NumberOfSourceRecords", sTemp)
            TryColAdd(aReturn, "CountOfRecords", sTemp)
            TryColAdd(aReturn, "CountOfSourceRecords", sTemp)
            TryColAdd(aReturn, "RecordsCount", sTemp)
            TryColAdd(aReturn, "SourceRecordsCount", sTemp)

            sTemp = "0"
            sTemp2 = "-1"
            If oCurrRecord IsNot Nothing Then
                iIndexOf = oCurrRecord.IndexOfMe
                sTemp = (iIndexOf + 1).ToString
                sTemp2 = iIndexOf.ToString
            End If
            TryColAdd(aReturn, "RecordNo", sTemp)
            TryColAdd(aReturn, "RecNo", sTemp)
            TryColAdd(aReturn, "RecordNumber", sTemp)
            TryColAdd(aReturn, "CurrRecordNo", sTemp)
            TryColAdd(aReturn, "CurrRecNo", sTemp)
            TryColAdd(aReturn, "CurrRecordNumber", sTemp)
            TryColAdd(aReturn, "CurrentRecordNo", sTemp)
            TryColAdd(aReturn, "CurrentRecNo", sTemp)
            TryColAdd(aReturn, "CurrentRecordNumber", sTemp)
            TryColAdd(aReturn, "IndexNo", sTemp2)
            TryColAdd(aReturn, "IndexNumber", sTemp2)
            TryColAdd(aReturn, "CurrIndexNo", sTemp2)
            TryColAdd(aReturn, "CurrIndexNumber", sTemp2)
            TryColAdd(aReturn, "CurrentIndexNo", sTemp2)
            TryColAdd(aReturn, "CurrentIndexNumber", sTemp2)


        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            RaiseEvent onError(Me, oErrItem)
        End Try
        Return aReturn
    End Function

    Public Function SubstitutedValue(sSourceString As String) As String
        Dim sReturn As String = sSourceString, oSubst As clsValueSubstitutions
        Try

            oSubst = New clsValueSubstitutions(sReturn, Me.DetailsAsSortedList)

            sReturn = oSubst.Result

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            RaiseEvent onError(Me, oErrItem)

        End Try
        Return sReturn
    End Function


End Class



''' <summary>
''' Class to represent a single imported transaction
''' Based on BACS format
''' </summary>

Public Class clsOneSourcePaymentRecord
    Implements IComparable(Of clsOneSourcePaymentRecord)

    Public DestSortCode As String
    Public DestAccNo As String
    Public DestAccType As String
    Public TransCode As String
    Public OrigSortCode As String
    Public OrigAccNo As String
    Public OrigAccType As String
    Public UDDReturnCode As String
    Public ProcessingDate As Date
    Public TDate As Date
    Public TAmt As Decimal
    Public DocID As String
    Public TCode As String
    Public BillRef As String
    Public PayDate As Date
    Public SrcStringIn As String

    Public iSourceRecordNo As Integer
    Public oParentCol As clsFileLoadByType = Nothing
    Public OrigLine As String = ""

    Protected Friend sOutputFund As String = ""
    Protected Friend sOutputFundReference As String = ""
    Protected Friend sOutputAmount As Double = 0
    Protected Friend sOutputEffectiveDate As Date = Nothing
    Protected Friend sOutputDatePaid As Date = Nothing
    Protected Friend sOutputMOPType As String = ""
    Protected Friend sOutputReceiptRef As String = ""
    Protected Friend sOutputPayName As String = ""
    Protected Friend sOutputPayNotes As String = ""
    Protected Friend sOutputCashPayRef As String = ""

    Public Property OutputFund As String
        Get
            Return sOutputFund
        End Get
        Set(value As String)
            sOutputFund = value
        End Set
    End Property
    Public Property OutputFundReference As String
        Get
            Return sOutputFundReference
        End Get
        Set(value As String)
            sOutputFundReference = value
        End Set
    End Property
    Public Property OutputAmount As Double
        Get
            Return sOutputAmount
        End Get
        Set(value As Double)
            sOutputAmount = value
        End Set
    End Property
    Public Property OutputEffectiveDate As Date
        Get
            Return sOutputEffectiveDate
        End Get
        Set(value As Date)
            sOutputEffectiveDate = value
        End Set
    End Property
    Public Property OutputDatePaid As Date
        Get
            Return sOutputDatePaid
        End Get
        Set(value As Date)
            sOutputDatePaid = value
        End Set
    End Property
    Public Property OutputMOPType As String
        Get
            Return sOutputMOPType
        End Get
        Set(value As String)
            sOutputMOPType = value
        End Set
    End Property
    Public Property OutputReceiptRef As String
        Get
            Return sOutputReceiptRef
        End Get
        Set(value As String)
            sOutputReceiptRef = value
        End Set
    End Property
    Public Property OutputPayName As String
        Get
            Return sOutputPayName
        End Get
        Set(value As String)
            sOutputPayName = value
        End Set
    End Property
    Public Property OutputPayNotes As String
        Get
            Return sOutputPayNotes
        End Get
        Set(value As String)
            sOutputPayNotes = value
        End Set
    End Property
    Public Property OutputCashPayRef As String
        Get
            Return sOutputCashPayRef
        End Get
        Set(value As String)
            sOutputCashPayRef = value
        End Set
    End Property


    Public Sub New()

    End Sub
    Public Sub New(oParent As clsFileLoadByType)
        Me.oParentCol = oParent
    End Sub

    Public Sub New(sBacsTransLine As String, oParent As clsFileLoadByType)
        Me.oParentCol = oParent
        Me.SourceBACSTransLine = sBacsTransLine
    End Sub


    Public Function PaymentLineOut() As String
        Dim aParts As New ArrayList, sOutput As String
        Dim sDelimUsed As String = ":", sDelimSubst As String = ";"
        Try

            For iPos = 0 To 9
                aParts.Add("")
            Next

            aParts(0) = OutputFund
            aParts(1) = OutputFundReference
            aParts(2) = OutputAmount.ToString("F2")
            aParts(3) = OutputEffectiveDate.ToString("dd/MM/yyyy")
            aParts(4) = OutputDatePaid.ToString("dd/MM/yyyy")
            aParts(5) = OutputMOPType
            aParts(6) = OutputReceiptRef
            aParts(7) = OutputPayName
            aParts(8) = OutputPayNotes
            aParts(9) = OutputCashPayRef

            For iPos = 0 To aParts.Count - 1
                aParts(iPos) = aParts(iPos).replace(sDelimUsed, sDelimSubst).replace(",", sDelimSubst)
            Next

            sOutput = Strings.Join(aParts.ToArray, sDelimSubst)

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return sOutput
    End Function


    Public Function SubstPaymentLineOut() As String
        Dim sReturn As String
        Try

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return sReturn
    End Function


    Public Function PaymntIOsValid() As Boolean
        Dim bReturn As Boolean = False
        Dim aCheck As ArrayList, sTemp As String
        Try
            bReturn = OutputAmount > 0
            bReturn = bReturn And OutputFund <> ""
            bReturn = bReturn And OutputFundReference <> ""
            bReturn = bReturn And OutputDatePaid.Ticks <> 0

            If bReturn Then
                Select Case oParentCol.oLoadManager.eFileType
                    Case clsFileLoadByType.eLoadTypes.BACS

                    Case clsFileLoadByType.eLoadTypes.Fastpay

                    Case clsFileLoadByType.eLoadTypes.Giro
                        sTemp = goMainSettings.GetFirstStringValueOfShortKey("GiroValidAccounts")
                        aCheck = New ArrayList(Split(sTemp, ","))
                        bReturn = aCheck.Contains(DestAccNo)

                    Case clsFileLoadByType.eLoadTypes.HOCA
                        bReturn = bReturn And (OrigLine.Length > 101 AndAlso IsNumeric(OrigLine.Substring(102, 5)))

                    Case clsFileLoadByType.eLoadTypes.UDD
                        bReturn = bReturn And (OrigLine.Length > 6 AndAlso IsNumeric(OrigLine.Substring(0, 6)))

                End Select
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return bReturn
    End Function


    Public Property SourceTransLine As String
        Get
            Dim eType As clsFileLoadByType.eLoadTypes
            Dim sReturn As String = ""

            Try

                eType = oParentCol.oLoadManager.eFileType
                Select Case eType
                    Case clsFileLoadByType.eLoadTypes.BACS : sReturn = SourceBACSTransLine
                    Case clsFileLoadByType.eLoadTypes.Fastpay : sReturn = SourceFastpayTransLine
                    Case clsFileLoadByType.eLoadTypes.Giro : sReturn = SourceGiroTransLine
                    Case clsFileLoadByType.eLoadTypes.HOCA : sReturn = SourceHOCATransLine
                    'Case clsFileLoadByType.eLoadTypes.Santander : sReturn = SourceSantanderTransLine
                    Case clsFileLoadByType.eLoadTypes.UDD : sReturn = SourceUDDTransLine
                    Case Else : sReturn = SourceBACSTransLine
                End Select

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

            End Try

            Return sReturn

        End Get
        Set(value As String)
            Dim eType As clsFileLoadByType.eLoadTypes

            Try

                eType = oParentCol.oLoadManager.eFileType
                Select Case eType
                    Case clsFileLoadByType.eLoadTypes.BACS : SourceBACSTransLine = value
                    Case clsFileLoadByType.eLoadTypes.Fastpay : SourceFastpayTransLine = value
                    Case clsFileLoadByType.eLoadTypes.Giro : SourceGiroTransLine = value
                    Case clsFileLoadByType.eLoadTypes.HOCA : SourceHOCATransLine = value
                    'Case clsFileLoadByType.eLoadTypes.Santander : SourceSantanderTransLine = value
                    Case clsFileLoadByType.eLoadTypes.UDD : SourceUDDTransLine = value
                    Case Else
                End Select

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

            End Try

        End Set
    End Property


    Public Property SourceBACSTransLine As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(160)
                sReturn = sReturn.Substring(0, 1) &
                    PadToLength(DestSortCode, 6, True) &
                    PadToLength(DestAccNo, 8, True) &
                    PadToLength(DestAccType, 1, True) &
                    PadToLength(TransCode, 2, True) &
                    PadToLength(OrigSortCode, 6, True) &
                    PadToLength(OrigAccNo, 8, True) &
                    PadToLength(UDDReturnCode, 1, True) &
                    DatePart(DateInterval.DayOfYear, ProcessingDate).ToString("D3") &
                    CInt(TAmt * 100).ToString("D11")
                sReturn = sReturn.PadRight(64).Substring(0, 64) &
                    PadToLength(BillRef, 18) &
                    PadToLength(DocID, 18)

                sReturn = sReturn.PadRight(IIf(OrigLine = "", 160, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn

        End Get
        Set(value As String)
            Try
                OrigLine = value
                Dim sTemp As String = ""
                If value <> "" Then
                    value = value.PadRight(100)
                    DestSortCode = value.Substring(1, 6)
                    DestAccNo = value.Substring(7, 8)
                    DestAccType = value.Substring(15, 1)
                    TransCode = value.Substring(16, 2)
                    OrigSortCode = value.Substring(18, 6)
                    OrigAccNo = value.Substring(24, 8)
                    OrigAccType = value.Substring(32, 1)
                    UDDReturnCode = value.Substring(32, 1)
                    sTemp = value.Substring(33, 3)
                    If Val(sTemp) > DatePart(DateInterval.DayOfYear, oParentCol.HDR1Rec.CreateDate) + 300 Then
                        ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) - 1 & CInt(Trim(sTemp)).ToString("D3")
                    Else
                        ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) & CInt(Trim(sTemp)).ToString("D3")
                    End If
                    '                  .TDate = JulianToDate(Mid$(strCurrLine, 59, 5))
                    TDate = oParentCol.HDR1Rec.CreateDate
                    TAmt = 0 - CDec(value.Substring(36, 11)) / 100
                    DocID = Trim(value.Substring(83, 18))
                    TCode = TransCode
                    PayDate = DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, oParentCol.HDR1Rec.CreateDate)
                    BillRef = Trim(value.Substring(65, 18))
                End If
            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
        End Set
    End Property


    Public Property SourceFastpayTransLine As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(160)
                sReturn = sReturn.Substring(0, 1) &
                    PadToLength(DestSortCode, 6, True) &
                    PadToLength(DestAccNo, 8, True) &
                    PadToLength(DestAccType, 1, True) &
                    PadToLength(TransCode, 2, True) &
                    PadToLength(OrigSortCode, 6, True) &
                    PadToLength(OrigAccNo, 8, True) &
                    PadToLength(UDDReturnCode, 1, True) &
                    DatePart(DateInterval.DayOfYear, ProcessingDate).ToString("D3") &
                    CInt(TAmt * 100).ToString("D11")
                sReturn = sReturn.PadRight(64).Substring(0, 64) &
                    PadToLength(BillRef, 18) &
                    PadToLength(DocID, 18)

                sReturn = sReturn.PadRight(IIf(OrigLine = "", 160, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn

        End Get
        Set(value As String)
            Try
                OrigLine = value
                Dim sTemp As String = ""
                If value <> "" Then
                    value = value.PadRight(100)
                    DestSortCode = value.Substring(1, 6)
                    DestAccNo = value.Substring(7, 8)
                    DestAccType = value.Substring(15, 1)
                    TransCode = value.Substring(16, 2)
                    OrigSortCode = value.Substring(18, 6)
                    OrigAccNo = value.Substring(24, 8)
                    OrigAccType = value.Substring(32, 1)
                    DocID = Trim(value.Substring(47, 18))
                    ProcessingDate = oParentCol.HDR1Rec.CreateDate
                    'UDDReturnCode = value.Substring(32, 1)
                    'sTemp = value.Substring(33, 3)

                    'If Val(sTemp) > DatePart(DateInterval.DayOfYear, oParentCol.HDR1Rec.CreateDate) + 300 Then
                    '    ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) - 1 & CInt(Trim(sTemp)).ToString("D3")
                    'Else
                    '    ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) & CInt(Trim(sTemp)).ToString("D3")
                    'End If
                    '                  .TDate = JulianToDate(Mid$(strCurrLine, 59, 5))
                    TDate = oParentCol.HDR1Rec.CreateDate
                    TAmt = 0 - CDec(value.Substring(36, 11)) / 100
                    TCode = TransCode
                    PayDate = DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, oParentCol.HDR1Rec.CreateDate)
                    BillRef = Trim(value.Substring(65, 18))
                End If
            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
        End Set
    End Property

    Public Property SourceGiroTransLine As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(160)
                sReturn = sReturn.Substring(0, 1) &
                    PadToLength(DestSortCode, 6, True) &
                    PadToLength(DestAccNo, 8, True) &
                    PadToLength(DestAccType, 1, True) &
                    PadToLength(TransCode, 2, True) &
                    PadToLength(OrigSortCode, 6, True) &
                    PadToLength(OrigAccNo, 8, True) &
                    PadToLength(UDDReturnCode, 1, True) &
                    DatePart(DateInterval.DayOfYear, ProcessingDate).ToString("D3") &
                    CInt(TAmt * 100).ToString("D11")
                sReturn = sReturn.PadRight(64).Substring(0, 64) &
                    PadToLength(BillRef, 18) &
                    PadToLength(DocID, 18)

                sReturn = sReturn.PadRight(IIf(OrigLine = "", 160, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn

        End Get
        Set(value As String)
            Dim sTemp As String = ""
            Try
                OrigLine = value
                If value <> "" Then
                    value = value.PadRight(100)
                    DestSortCode = value.Substring(1, 6)
                    DestAccNo = value.Substring(7, 8)
                    DestAccType = value.Substring(15, 1)
                    TransCode = value.Substring(16, 2)
                    OrigSortCode = value.Substring(18, 6)
                    OrigAccNo = value.Substring(24, 8)
                    OrigAccType = value.Substring(32, 1)
                    UDDReturnCode = value.Substring(32, 1)
                    sTemp = value.Substring(33, 3)
                    If Val(sTemp) > DatePart(DateInterval.DayOfYear, oParentCol.HDR1Rec.CreateDate) + 300 Then
                        ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) - 1 & CInt(Trim(sTemp)).ToString("D3")
                    Else
                        ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) & CInt(Trim(sTemp)).ToString("D3")
                    End If
                    '                  .TDate = JulianToDate(Mid$(strCurrLine, 59, 5))
                    TDate = oParentCol.HDR1Rec.CreateDate
                    TAmt = 0 - CDec(value.Substring(36, 11)) / 100
                    DocID = Trim(value.Substring(83, 18))
                    TCode = TransCode
                    PayDate = DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, oParentCol.HDR1Rec.CreateDate)
                    BillRef = Trim(value.Substring(65, 18))
                End If
            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
        End Set
    End Property

    Public Property SourceHOCATransLine As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(160)
                sReturn = sReturn.Substring(0, 1) &
                    PadToLength(DestSortCode, 6, True) &
                    PadToLength(DestAccNo, 8, True) &
                    PadToLength(DestAccType, 1, True) &
                    PadToLength(TransCode, 2, True) &
                    PadToLength(OrigSortCode, 6, True) &
                    PadToLength(OrigAccNo, 8, True) &
                    PadToLength(UDDReturnCode, 1, True) &
                    DatePart(DateInterval.DayOfYear, ProcessingDate).ToString("D3") &
                    CInt(TAmt * 100).ToString("D11")
                sReturn = sReturn.PadRight(64).Substring(0, 64) &
                    PadToLength(BillRef, 18) &
                    PadToLength(DocID, 18)

                sReturn = sReturn.PadRight(IIf(OrigLine = "", 160, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn

        End Get
        Set(value As String)
            Dim sTemp As String = "", bJointGiro As Boolean
            Try
                OrigLine = value
                If value <> "" Then
                    value = value.PadRight(160)
                    bJointGiro = (value.Substring(0, 3) = "   ")
                    If bJointGiro Then
                        DestSortCode = value.Substring(10, 6)
                        DestAccNo = value.Substring(3, 7)
                    Else
                        DestSortCode = value.Substring(1, 6)
                        DestAccNo = value.Substring(7, 8)
                    End If
                    DestAccType = value.Substring(15, 1)
                    TransCode = value.Substring(16, 2)
                    OrigSortCode = value.Substring(18, 6)
                    OrigAccNo = value.Substring(24, 8)
                    OrigAccType = value.Substring(32, 1)
                    UDDReturnCode = value.Substring(32, 1)
                    sTemp = value.Substring(33, 3)
                    TAmt = 0 - CDec(value.Substring(36, 11)) / 100
                    DocID = Trim(value.Substring(47, 18))
                    BillRef = Trim(value.Substring(65, 18))
                    TDate = JulianToDate(value.Substring(101, 5))

                    TCode = TransCode
                    PayDate = TDate
                End If
            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
        End Set
    End Property

    'Public Property SourceSantanderTransLine As String
    '    Get
    '        Dim sReturn As String = ""
    '        Try
    '            sReturn = Space(160)
    '            sReturn = sReturn.Substring(0, 1) &
    '                PadToLength(DestSortCode, 6, True) &
    '                PadToLength(DestAccNo, 8, True) &
    '                PadToLength(DestAccType, 1, True) &
    '                PadToLength(TransCode, 2, True) &
    '                PadToLength(OrigSortCode, 6, True) &
    '                PadToLength(OrigAccNo, 8, True) &
    '                PadToLength(UDDReturnCode, 1, True) &
    '                DatePart(DateInterval.DayOfYear, ProcessingDate).ToString("D3") &
    '                CInt(TAmt * 100).ToString("D11")
    '            sReturn = sReturn.PadRight(64).Substring(0, 64) &
    '                PadToLength(BillRef, 18) &
    '                PadToLength(DocID, 18)

    '            sReturn = sReturn.PadRight(IIf(OrigLine = "", 160, OrigLine.Length))

    '        Catch ex As Exception
    '            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
    '        End Try
    '        Return sReturn

    '    End Get
    '    Set(value As String)
    '        Dim sTemp As String = ""
    '        Try
    '            OrigLine = value
    '            If value <> "" Then
    '                value = value.PadRight(100)
    '                DestSortCode = value.Substring(1, 6)
    '                DestAccNo = value.Substring(7, 8)
    '                DestAccType = value.Substring(15, 1)
    '                TransCode = value.Substring(16, 2)
    '                OrigSortCode = value.Substring(18, 6)
    '                OrigAccNo = value.Substring(24, 8)
    '                OrigAccType = value.Substring(32, 1)
    '                UDDReturnCode = value.Substring(32, 1)
    '                sTemp = value.Substring(33, 3)
    '                If Val(sTemp) > DatePart(DateInterval.DayOfYear, oParentCol.HDR1Rec.CreateDate) + 300 Then
    '                    ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) - 1 & CInt(Trim(sTemp)).ToString("D3")
    '                Else
    '                    ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) & CInt(Trim(sTemp)).ToString("D3")
    '                End If
    '                '                  .TDate = JulianToDate(Mid$(strCurrLine, 59, 5))
    '                TDate = oParentCol.HDR1Rec.CreateDate
    '                TAmt = 0 - CDec(value.Substring(36, 11)) / 100
    '                DocID = Trim(value.Substring(83, 18))
    '                TCode = TransCode
    '                PayDate = DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, oParentCol.HDR1Rec.CreateDate)
    '                BillRef = Trim(value.Substring(65, 18))
    '            End If
    '        Catch ex As Exception
    '            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
    '        End Try
    '    End Set
    'End Property

    Public Property SourceUDDTransLine As String
        Get
            Dim sReturn As String = ""
            Try
                sReturn = Space(160)
                sReturn = sReturn.Substring(0, 1) &
                    PadToLength(DestSortCode, 6, True) &
                    PadToLength(DestAccNo, 8, True) &
                    PadToLength(DestAccType, 1, True) &
                    PadToLength(TransCode, 2, True) &
                    PadToLength(OrigSortCode, 6, True) &
                    PadToLength(OrigAccNo, 8, True) &
                    PadToLength(UDDReturnCode, 1, True) &
                    DatePart(DateInterval.DayOfYear, ProcessingDate).ToString("D3") &
                    CInt(TAmt * 100).ToString("D11")
                sReturn = sReturn.PadRight(64).Substring(0, 64) &
                    PadToLength(BillRef, 18) &
                    PadToLength(DocID, 18)

                sReturn = sReturn.PadRight(IIf(OrigLine = "", 160, OrigLine.Length))

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
            Return sReturn

        End Get
        Set(value As String)
            Dim sTemp As String = ""
            Try
                OrigLine = value
                If value <> "" Then
                    value = value.PadRight(100)
                    DestSortCode = value.Substring(1, 6)
                    DestAccNo = value.Substring(7, 8)
                    DestAccType = value.Substring(15, 1)
                    TransCode = value.Substring(16, 2)
                    OrigSortCode = value.Substring(18, 6)
                    OrigAccNo = value.Substring(24, 8)
                    OrigAccType = value.Substring(32, 1)
                    UDDReturnCode = value.Substring(32, 1)
                    sTemp = value.Substring(33, 3)
                    If Val(sTemp) > DatePart(DateInterval.DayOfYear, oParentCol.HDR1Rec.CreateDate) + 300 Then
                        ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) - 1 & CInt(Trim(sTemp)).ToString("D3")
                    Else
                        ProcessingDate = DatePart(DateInterval.Year, oParentCol.HDR1Rec.CreateDate) & CInt(Trim(sTemp)).ToString("D3")
                    End If
                    '                  .TDate = JulianToDate(Mid$(strCurrLine, 59, 5))
                    TDate = oParentCol.HDR1Rec.CreateDate
                    TAmt = 0 - CDec(value.Substring(36, 11)) / 100
                    DocID = Trim(value.Substring(83, 18))
                    TCode = TransCode
                    PayDate = DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, oParentCol.HDR1Rec.CreateDate)
                    BillRef = Trim(value.Substring(65, 18))
                End If
            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
            End Try
        End Set
    End Property



    Protected Friend Function DetailsAsSortedList() As SortedList(Of String, String)
        Dim aReturn As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
        Dim sVal As String, sVal2 As String, sTemp As String, sTemp2 As String, iIndexOf As Integer
        Try

            If oParentCol IsNot Nothing Then aReturn = oParentCol.DetailsAsSortedList

            TryColAdd(aReturn, "Fund", OutputFund)
            TryColAdd(aReturn, "FundCode", OutputFund)
            TryColAdd(aReturn, "FundID", OutputFund)
            TryColAdd(aReturn, "FundReference", OutputFundReference)
            TryColAdd(aReturn, "Reference", OutputFundReference)
            TryColAdd(aReturn, "Amount", OutputAmount.ToString("F2"))
            TryColAdd(aReturn, "EffectiveDate", OutputEffectiveDate.ToString("dd MMM yyyy"))
            TryColAdd(aReturn, "DatePaid", OutputDatePaid.ToString("dd MMM yyyy"))
            TryColAdd(aReturn, "MOP", OutputMOPType)
            TryColAdd(aReturn, "MOPType", OutputMOPType)
            TryColAdd(aReturn, "ReceiptRef", OutputReceiptRef)
            TryColAdd(aReturn, "PayName", OutputPayName)
            TryColAdd(aReturn, "PayeeName", OutputPayName)
            TryColAdd(aReturn, "Name", OutputPayName)
            TryColAdd(aReturn, "PayNotes", OutputPayNotes)
            TryColAdd(aReturn, "PayeeNotes", OutputPayNotes)
            TryColAdd(aReturn, "Notes", OutputPayNotes)
            TryColAdd(aReturn, "CashPayRef", OutputCashPayRef)

            sTemp = iSourceRecordNo.ToString
            TryColAdd(aReturn, "SourceRecordNo", sTemp)
            TryColAdd(aReturn, "SourceRecNo", sTemp)
            TryColAdd(aReturn, "SourceRecordNumber", sTemp)

            iIndexOf = IndexOfMe()
            sTemp = (iIndexOf - 1).ToString
            sTemp2 = IndexOfMe.ToString
            TryColAdd(aReturn, "RecordNo", sTemp)
            TryColAdd(aReturn, "RecNo", sTemp)
            TryColAdd(aReturn, "RecordNumber", sTemp)
            TryColAdd(aReturn, "CurrRecordNo", sTemp)
            TryColAdd(aReturn, "CurrRecNo", sTemp)
            TryColAdd(aReturn, "CurrRecordNumber", sTemp)
            TryColAdd(aReturn, "CurrentRecordNo", sTemp)
            TryColAdd(aReturn, "CurrentRecNo", sTemp)
            TryColAdd(aReturn, "CurrentRecordNumber", sTemp)
            TryColAdd(aReturn, "IndexNo", sTemp2)
            TryColAdd(aReturn, "IndexNumber", sTemp2)
            TryColAdd(aReturn, "CurrIndexNo", sTemp2)
            TryColAdd(aReturn, "CurrIndexNumber", sTemp2)
            TryColAdd(aReturn, "CurrentIndexNo", sTemp2)
            TryColAdd(aReturn, "CurrentIndexNumber", sTemp2)


        Catch ex As Exception

            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
        End Try
        Return aReturn
    End Function


    Public Function IndexOfMe() As Integer
        Dim iReturn As Integer = -1
        Try
            If oParentCol IsNot Nothing Then iReturn = oParentCol.IndexOf(Me)
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return iReturn
    End Function


    Public Function SubstitutedValue(sSourceString As String) As String
        Dim sReturn As String = sSourceString, oSubst As clsValueSubstitutions
        Try

            oSubst = New clsValueSubstitutions(sReturn, Me.DetailsAsSortedList)
            sReturn = oSubst.Result

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return sReturn
    End Function




    Public Function SortIndex() As String
        Dim eBy As clsFileLoadByType.SortPayments = clsFileLoadByType.SortPayments.ByFundReference
        Dim sReturn As String = ""

        Try

            If oParentCol IsNot Nothing Then eBy = oParentCol.SortBy
            Select Case eBy
                Case clsFileLoadByType.SortPayments.ByFundReference
                    sReturn = OutputFund.PadRight(8) & OutputCashPayRef.PadLeft(20) & OutputDatePaid.ToString("o")
                Case clsFileLoadByType.SortPayments.ByPostDate
                    sReturn = OutputDatePaid.ToString("o") & OutputFund.PadRight(8) & OutputCashPayRef.PadLeft(20)
                Case clsFileLoadByType.SortPayments.BySource
                    sReturn = OutputMOPType.PadRight(8) & OutputFund.PadRight(8) & OutputCashPayRef.PadLeft(20) & OutputDatePaid.ToString("o")
                Case Else
                    sReturn = OutputFund.PadRight(8) & OutputCashPayRef.PadLeft(20) & OutputDatePaid.ToString("o")
            End Select
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

        Return sReturn
    End Function

    Public Function CompareTo(other As clsOneSourcePaymentRecord) As Integer Implements IComparable(Of clsOneSourcePaymentRecord).CompareTo
        Dim iReturn As Integer = 0, eBy As clsFileLoadByType.SortPayments = clsFileLoadByType.SortPayments.ByFundReference

        Try

            iReturn = Me.SortIndex.CompareTo(other.SortIndex)
            If oParentCol IsNot Nothing AndAlso oParentCol.bLowestFirstSortOrder Then iReturn = -iReturn

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return iReturn
    End Function
End Class


