﻿Imports System.Xml
Imports CashExp2018

Public Class clsFundIdentifier
    Inherits List(Of clsOneFundID)

    Public Function AddIdentifier(oIDOfFund As clsOneFundID) As clsOneFundID
        Dim oReturn As clsOneFundID = oIDOfFund, oTest As clsOneFundID
        Try

            If Not Me.Contains(oIDOfFund) Then
                oTest = FindByStringRegex(oIDOfFund.RegexToMatch)
                If oTest IsNot Nothing Then
                    oReturn = oTest
                Else
                    oReturn = oIDOfFund
                    oReturn.OrderPosition = Me.Count + 1
                    Me.Add(oReturn)
                End If
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Function

    Public Function FindByStringRegex(sSearchFor As String) As clsOneFundID
        Dim oReturn As clsOneFundID = Nothing, oTest As clsOneFundID = Nothing
        Try

            For Each oTest In Me
                If oTest.RegexToMatch = sSearchFor Then
                    oReturn = oTest
                    Exit For
                End If
            Next

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return oReturn
    End Function

    Public Sub LoadFromXML(XMLDom As XmlDocument)
        Dim oRoot As XmlElement, oNode As XmlNode, oElem As XmlElement, oAttr As XmlAttribute
        Dim sNodeSearchXPath As String, aFundIDs As XmlNodeList
        Dim oFundID As clsOneFundID

        Try
            sNodeSearchXPath = GetMySettingString("FundsNodeSearchXPath")

            oRoot = XMLDom.DocumentElement
            aFundIDs = oRoot.SelectNodes(sNodeSearchXPath)

            For Each oNode In aFundIDs
                oElem = TryCast(oNode, XmlElement)
                If oElem IsNot Nothing Then
                    oFundID = New clsOneFundID(oElem)
                    Me.Add(oFundID)
                End If
            Next

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub

End Class

Public Class clsOneFundID
    Implements IComparable(Of clsOneFundID)

    Public Property OrderPosition As Integer
    Public Property RegexToMatch As String
    Public Property FundID As String
    Public Property RegexReplace As String
    Public Property StartPos As Integer = 0
    Public Property NewRefLength As Integer = 0

    Public Sub New()

    End Sub

    Public Sub New(oNode As XmlNode)
        SetValuesFromXML(oNode)
    End Sub


    Public Sub SetValuesFromXML(oElemNode As XmlElement)
        Dim sXPathFindRegex As String, sXPathFindFundID As String, sXPathFindReplace As String
        Dim sXPathFindLeftPos As String, sXPathFindLength As String, sTemp As String
        Dim oNode As XmlNode, oElem As XmlElement, oAttr As XmlAttribute, oXMLText As XmlText
        Try

            sXPathFindRegex = GetMySettingString("XPathFundFindRegex")
            oNode = oElemNode.SelectSingleNode(sXPathFindRegex)
            If oNode IsNot Nothing Then RegexToMatch = ValOfNode(oNode)

            sXPathFindFundID = GetMySettingString("XPathFundFindFundID")
            oNode = oElemNode.SelectSingleNode(sXPathFindFundID)
            If oNode IsNot Nothing Then Fund = ValOfNode(oNode)

            sXPathFindReplace = GetMySettingString("XPathFundFindReplace")
            oNode = oElemNode.SelectSingleNode(sXPathFindReplace)
            If oNode IsNot Nothing Then RegexReplace = ValOfNode(oNode)

            sXPathFindLeftPos = GetMySettingString("XPathFundFindLeftPos")
            oNode = oElemNode.SelectSingleNode(sXPathFindLeftPos)
            If oNode IsNot Nothing Then
                sTemp = ValOfNode(oNode)
                If IsNumeric(sTemp) Then StartPos = CInt(Math.Floor(Val(sTemp)))
            End If

            sXPathFindLength = GetMySettingString("XPathFundFindLength")
            oNode = oElemNode.SelectSingleNode(sXPathFindLength)
            If oNode IsNot Nothing Then
                sTemp = ValOfNode(oNode)
                If IsNumeric(sTemp) Then NewRefLength = CInt(Math.Floor(Val(sTemp)))
            End If


        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub

    Public Function ValOfNode(oNode As XmlNode) As String
        Dim sReturn As String = ""
        Dim oElem As XmlElement, oAttr As XmlAttribute, oXMLText As XmlText
        Dim oChildNode As XmlNode
        Try
            oAttr = TryCast(oNode, XmlAttribute)
            If oAttr IsNot Nothing Then
                sReturn = oAttr.Value
            Else
                oElem = TryCast(oNode, XmlElement)
                If oElem IsNot Nothing Then
                    For Each oChildNode In oElem.ChildNodes
                        If TypeOf (oChildNode) Is XmlText Then
                            oXMLText = TryCast(oChildNode, XmlText)
                            sReturn = IIf(sReturn <> "", vbCrLf, "") & oXMLText.Value
                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return sReturn
    End Function

    Public Function CompareTo(other As clsOneFundID) As Integer Implements IComparable(Of clsOneFundID).CompareTo
        Return OrderPosition.CompareTo(other.OrderPosition)
    End Function
End Class
