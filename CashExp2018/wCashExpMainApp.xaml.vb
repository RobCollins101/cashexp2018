﻿Imports System.Text.RegularExpressions

Class wCashExpMainApp
    Dim bEnableDialog As Boolean = True

    ' =========================================================================
    ' Main window functions and subs to do local window operations
    ' =========================================================================


    Private Sub wCashExpMainApp_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded

        VerifyControls()

    End Sub


    ' =========================================================================
    ' Functions for various major command operations
    ' =========================================================================






    ' =========================================================================
    ' Tool like functions and subs to do local window operations
    ' =========================================================================


    Private Sub VerifyControls()
        Dim bMOPIsValid As Boolean, oSetting As clsLoadedElement
        Dim sMOPFolder As String, sMOPRegex As String
        Try


            sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("BACSSourceFolder")
            sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("BACSNameRegex")
            bMOPIsValid = VerifyFilesPresent(sMOPFolder, sMOPRegex)
            cmdDoBACS.IsEnabled = bMOPIsValid And bEnableDialog

            sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("FastpaySourceFolder")
            sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("FastpayNameRegex")
            bMOPIsValid = VerifyFilesPresent(sMOPFolder, sMOPRegex)
            cmdDoFastPay.IsEnabled = bMOPIsValid And bEnableDialog

            sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("GiroSourceFolder")
            sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("GiroNameRegex")
            bMOPIsValid = VerifyFilesPresent(sMOPFolder, sMOPRegex)
            cmdDoGiro.IsEnabled = bMOPIsValid And bEnableDialog

            sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("HOCASourceFolder")
            sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("HOCANameRegex")
            bMOPIsValid = VerifyFilesPresent(sMOPFolder, sMOPRegex)
            cmdDoHOCA.IsEnabled = bMOPIsValid And bEnableDialog

            sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("SantanderSourceFolder")
            sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("SantanderNameRegex")
            bMOPIsValid = VerifyFilesPresent(sMOPFolder, sMOPRegex)
            cmdDoSantander.IsEnabled = bMOPIsValid And bEnableDialog

            sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("UDDSourceFolder")
            sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("UDDNameRegex")
            bMOPIsValid = VerifyFilesPresent(sMOPFolder, sMOPRegex)
            cmdDoUDD.IsEnabled = bMOPIsValid And bEnableDialog

            cmdDoManualBatch.IsEnabled = bEnableDialog

            cmdGoProcessSettings.IsEnabled = bEnableDialog
            cmdGoTranslation.IsEnabled = bEnableDialog
            cmdDoExit.IsEnabled = True


        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

    End Sub


    Private Function VerifyFilesPresent(sFolderPath As String, sRegexMatch As String) As Boolean
        Dim bReturn As Boolean = False
        Dim sDirMatch As String, sFile As String, sFileName As String
        Try

            sDirMatch = GetMySettingString("DirectoryRegex")
            If sDirMatch = "" OrElse Regex.IsMatch(sFolderPath, sDirMatch) Then
                If IO.Directory.Exists(sFolderPath) Then
                    For Each sFile In IO.Directory.GetFiles(sFolderPath)
                        sFileName = IO.Path.GetFileName(sFile)
                        If sRegexMatch = "" OrElse Regex.IsMatch(sFileName, sRegexMatch) Then
                            bReturn = True
                            Exit For
                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return bReturn
    End Function

    Private Sub cmdDoUDD_Click(sender As Object, e As RoutedEventArgs) Handles cmdDoUDD.Click

        DoLoading(clsFileLoadByType.eLoadTypes.UDD)

    End Sub

    Private Sub cmdDoHOCA_Click(sender As Object, e As RoutedEventArgs) Handles cmdDoHOCA.Click
        DoLoading(clsFileLoadByType.eLoadTypes.HOCA)

    End Sub

    Private Sub cmdDoBACS_Click(sender As Object, e As RoutedEventArgs) Handles cmdDoBACS.Click
        DoLoading(clsFileLoadByType.eLoadTypes.BACS)

    End Sub

    Private Sub cmdDoFastPay_Click(sender As Object, e As RoutedEventArgs) Handles cmdDoFastPay.Click
        DoLoading(clsFileLoadByType.eLoadTypes.Fastpay)

    End Sub

    Private Sub cmdDoGiro_Click(sender As Object, e As RoutedEventArgs) Handles cmdDoGiro.Click
        DoLoading(clsFileLoadByType.eLoadTypes.Giro)

    End Sub

    Private Sub cmdDoSantander_Click(sender As Object, e As RoutedEventArgs) Handles cmdDoSantander.Click
        'DoLoading(clsFileLoadByType.eLoadTypes.Santander)

    End Sub

    Private Sub cmdDoManualBatch_Click(sender As Object, e As RoutedEventArgs) Handles cmdDoManualBatch.Click

    End Sub

    Private Sub cmdGoProcessSettings_Click(sender As Object, e As RoutedEventArgs) Handles cmdGoProcessSettings.Click

    End Sub

    Private Sub cmdGoTranslation_Click(sender As Object, e As RoutedEventArgs) Handles cmdGoTranslation.Click

    End Sub


    Private Sub cmdDoExit_Click(sender As Object, e As RoutedEventArgs) Handles cmdDoExit.Click

        DoClosing()

    End Sub


    ' =======================================================
    ' Processes to do actions from control triggers
    ' =======================================================


    Private Sub DoLoading(eType As clsFileLoadByType.eLoadTypes)
        Dim oLoadDialog As wLoadDataFile
        Dim sMOPFolder As String = "", sMOPRegex As String = ""
        Try

            bEnableDialog = False
            VerifyControls()

            Select Case eType
                Case clsFileLoadByType.eLoadTypes.BACS
                    sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("BACSSourceFolder")
                    sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("BACSNameRegex")

                Case clsFileLoadByType.eLoadTypes.Fastpay
                    sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("FastpaySourceFolder")
                    sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("FastpayNameRegex")

                Case clsFileLoadByType.eLoadTypes.Giro
                    sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("GiroSourceFolder")
                    sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("GiroNameRegex")

                Case clsFileLoadByType.eLoadTypes.HOCA
                    sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("HOCASourceFolder")
                    sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("HOCANameRegex")

                'Case clsFileLoadByType.eLoadTypes.Santander
                '    sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("SantanderSourceFolder")
                '    sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("SantanderNameRegex")

                Case clsFileLoadByType.eLoadTypes.UDD
                    sMOPFolder = goMainSettings.GetFirstStringValueOfShortKey("UDDSourceFolder")
                    sMOPRegex = goMainSettings.GetFirstStringValueOfShortKey("UDDNameRegex")

            End Select

            If sMOPFolder <> "" And sMOPRegex <> "" Then
                DimMyWindow()
                oLoadDialog = New wLoadDataFile()
                oLoadDialog.ShowWithSettings(sMOPFolder, sMOPRegex, eType)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
        Finally
            UndimMyWindow()
            bEnableDialog = True
            VerifyControls()
        End Try
    End Sub

    Private Sub DoClosing()
        Try

            Me.Close()

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
        End Try

    End Sub



    Private Sub DimMyWindow()
        Dim oTemp As Object, dblOpacity As Double = 0.7
        Try
            oTemp = GetMySetting("UnfocusWindowDimLevel") : If TypeOf (oTemp) Is Double Then dblOpacity = CDbl(oTemp)
            Me.Opacity = dblOpacity
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub


    Private Sub wCashExpMainApp_GotFocus(sender As Object, e As RoutedEventArgs) Handles Me.GotFocus
        Try
            Me.Opacity = 1
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub


    Private Sub UndimMyWindow()
        Dim dblOpacity As Double = 1 ', oTemp As Object
        Try
            Me.Opacity = dblOpacity
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub


    Protected Overrides Sub OnMouseLeftButtonDown(ByVal e As MouseButtonEventArgs)
        MyBase.OnMouseLeftButtonDown(e)

        ' Begin dragging the window
        Me.DragMove()
    End Sub


End Class
