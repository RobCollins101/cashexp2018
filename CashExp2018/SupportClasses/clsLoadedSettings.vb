﻿Imports System.IO
Imports System.Xml
Imports CashExp2018

Public Class clsLoadedSettings
    Inherits List(Of clsLoadedElement)
    Protected Friend oXMLDOM As XmlDocument
    Protected Friend sRootNodeName As String = "Root"
    Protected Friend oXMLFile As FileInfo
    Private sNodeNameDelimUsed As String = "|"


    Public Property sXMLFilePath As String
        Get
            Dim sReturn As String = ""
            If oXMLFile IsNot Nothing Then
                sReturn = oXMLFile.FullName
            End If
            Return sReturn
        End Get
        Set(value As String)
            Dim bNewObj As Boolean = False
            Try
                bNewObj = (oXMLFile Is Nothing)
                If oXMLFile Is Nothing OrElse Path.GetFullPath(value) <> oXMLFile.FullName Then bNewObj = True

                If bNewObj Then oXMLFile = New FileInfo(value)

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

            End Try

        End Set
    End Property

    Public Property XMLDOM As XmlDocument
        Get
            Try
                If oXMLDOM Is Nothing Then
                    oXMLDOM = New XmlDocument()
                    If oXMLFile.Exists Then
                        oXMLDOM.Load(oXMLFile.FullName)
                    End If
                End If

            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

            End Try
            Return oXMLDOM
        End Get
        Set(value As XmlDocument)
            oXMLDOM = value
        End Set
    End Property

    Public Sub LoadToList()
        Dim oFirstSetting As clsLoadedElement
        Dim oRoot As Xml.XmlElement
        Try

            oRoot = XMLDOM.DocumentElement
            sRootNodeName = oRoot.Name
            oFirstSetting = New clsLoadedElement()
            Me.Add(oFirstSetting)
            oFirstSetting.sShortKey = sRootNodeName

            AddElementsToList(oFirstSetting, oRoot)


        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub


    Public Function GetChildElements(oCurrElemValue As clsLoadedElement) As List(Of clsLoadedElement)
        Dim oReturn As New List(Of clsLoadedElement), oAllChildren As List(Of clsLoadedElement)
        Dim oElemValue As clsLoadedElement
        Try

            oAllChildren = GetValuesOfShortKey("", oCurrElemValue)
            For Each oElemValue In oAllChildren
                If oElemValue.ParentKey = oCurrElemValue.LongKey Then oAllChildren.Add(oElemValue)
            Next

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return oReturn
    End Function



    Public Sub SaveToXMLFile(sSavePath As String)
        Try
            sXMLFilePath = sSavePath
            SaveToXMLFile()

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub



    Public Sub SaveToXMLFile()
        Dim oDOMTree As XmlElement

        Try

            If oXMLFile IsNot Nothing Then
                oDOMTree = GenerateXMLRoot()
                oXMLDOM = New XmlDocument()
                oXMLDOM.LoadXml(oDOMTree.OuterXml)
                oXMLDOM.Save(oXMLFile.FullName)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub


    Public Function GenerateXMLRoot() As XmlElement
        Dim oReturn As XmlElement
        Dim oRootElemValue As clsLoadedElement
        Try

            oRootElemValue = Me.First
            oReturn = GenerateXMLElementFromParent(oRootElemValue)

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

        Return oReturn
    End Function


    Public Function GenerateXMLElementFromParent(oCurrElemValue As clsLoadedElement) As XmlElement
        Dim oReturn As XmlElement = Nothing, oChildXMLAttribute As XmlAttribute, oChildXMLElement As XmlElement
        Dim oChildren As List(Of clsLoadedElement), oChildElemValue As clsLoadedElement
        Dim oTextNode As XmlText

        Try
            If oCurrElemValue.bIsElement Then
                oReturn = oXMLDOM.CreateElement(oCurrElemValue.sShortKey)
                oReturn.InnerText = oCurrElemValue.oValue.ToString

                oChildren = GetChildElements(oCurrElemValue)

                For Each oChildElemValue In oChildren

                    If oChildElemValue.bIsElement Then
                        oChildXMLElement = GenerateXMLElementFromParent(oChildElemValue)
                        oTextNode = oXMLDOM.CreateTextNode(oChildElemValue.oValue.ToString)
                        oChildXMLElement.AppendChild(oTextNode)

                        oReturn.AppendChild(oChildXMLElement)
                    Else
                        oChildXMLAttribute = oXMLDOM.CreateAttribute(oChildElemValue.sShortKey)
                        oChildXMLAttribute.Value = oChildElemValue.oValue.ToString
                    End If
                Next
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return oReturn

    End Function


    Public Sub AddElementsToList(oParentElement As clsLoadedElement, oNode As XmlNode)
        Dim oChildNode As XmlNode, sValue As String, oTemp As Object, oChildOfElemNode As XmlNode
        Dim sShortKey As String = "", sShortKeyUsed As String = ""
        Dim oNodeElement As XmlElement, oNodeAttr As XmlAttribute, oNodeText As XmlText
        Dim iNodeUniqueCount As Integer = 0, oNewLoadedElement As clsLoadedElement
        Try

            For Each oChildNode In oNode.ChildNodes
                oNodeElement = TryCast(oChildNode, XmlElement)
                oNodeAttr = TryCast(oChildNode, XmlAttribute)
                sValue = ""
                If oNodeElement IsNot Nothing Then
                    sShortKey = oNodeElement.Name
                    For Each oChildOfElemNode In oNodeElement
                        If TypeOf oChildOfElemNode Is XmlText Then
                            oNodeText = TryCast(oChildOfElemNode, XmlText)
                            If oNodeText IsNot Nothing Then sValue &= IIf(sValue <> "", vbCrLf, "") & oNodeText.Value
                        End If
                    Next
                    'sValue = oNodeElement.InnerText
                ElseIf oNodeAttr IsNot Nothing Then
                    sShortKey = oNodeAttr.Name
                    sValue = oNodeAttr.Value
                Else
                    oTemp = oChildNode
                End If
                If sShortKey <> "" Then
                    oNewLoadedElement = New clsLoadedElement(oParentElement.LongKey)
                    oNewLoadedElement.bIsElement = (oNodeElement IsNot Nothing)
                    oNewLoadedElement.XMLNamespaceURI = oChildNode.NamespaceURI
                    If oChildNode.ParentNode IsNot Nothing Then oNewLoadedElement.ParentNodeName = oChildNode.ParentNode.LocalName
                    oNewLoadedElement.sShortKey = sShortKey
                    oNewLoadedElement.oValue = sValue
                    oNewLoadedElement.ParentElement = oParentElement

                    sShortKeyUsed = sShortKey
                    Do
                        iNodeUniqueCount += 1
                        'If iNodeCount > 0 Then sShortKeyUsed = sShortKey & ":" & iNodeCount.ToString("D5")
                        oNewLoadedElement.iShortKeyCount = iNodeUniqueCount
                    Loop While Me.LongKeyExists(oNewLoadedElement.LongKey) And Not Me.Contains(oNewLoadedElement)

                    Me.Add(oNewLoadedElement)
                    oParentElement.aChildElements.Add(oNewLoadedElement)

                    If oNewLoadedElement.bIsElement Then AddElementsToList(oNewLoadedElement, oNodeElement)
                End If

            Next

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
    End Sub


    Public Function LongKeyExists(sNodeKey As String)
        Dim bReturn As Boolean = False, oVal As clsLoadedElement
        For Each oVal In Me
            If oVal.LongKey = sNodeKey Then
                bReturn = True
                Exit For
            End If
        Next
        Return bReturn
    End Function

    Public Function GetValuesOfShortKey(sShortKey As String, Optional oParentElement As clsLoadedElement = Nothing) As List(Of clsLoadedElement)
        Dim oReturn As New List(Of clsLoadedElement), oTestElement As clsLoadedElement

        Try

            For Each oTestElement In Me
                If oParentElement Is Nothing OrElse oTestElement.LongKey.IndexOf(oParentElement.LongKey) = 0 Then
                    If oTestElement.sShortKey = sShortKey Then oReturn.Add(oTestElement)
                ElseIf (sShortKey = "" And oParentElement IsNot Nothing) AndAlso oTestElement.LongKey.IndexOf(oParentElement.LongKey) = 0 Then
                    oReturn.Add(oTestElement)
                End If

            Next

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

        Return oReturn
    End Function

    Public Function GetFirstValueOfShortKey(sShortKey As String, Optional oParentElement As clsLoadedElement = Nothing) As clsLoadedElement
        Dim oReturn As clsLoadedElement = Nothing, oList As List(Of clsLoadedElement)

        Try
            oList = GetValuesOfShortKey(sShortKey, oParentElement)
            If oList IsNot Nothing AndAlso oList.Count > 0 Then oReturn = oList.Item(0)
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return oReturn
    End Function

    Public Function GetFirstStringValueOfShortKey(sShortKey As String, Optional oParentElement As clsLoadedElement = Nothing) As String
        Dim sReturn As String = "", oElem As clsLoadedElement = Nothing

        Try
            oElem = GetFirstValueOfShortKey(sShortKey, oParentElement)
            If oElem IsNot Nothing Then sReturn = oElem.oValue.ToString

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return sReturn
    End Function



    Public Function WriteASetting(sLongPath As String, oParentElemValue As clsLoadedElement, sValue As String, Optional bIsElement As Boolean = True) As clsLoadedElement
        Dim oReturn As clsLoadedElement = Nothing, sCurrentKeyValue As String
        Dim aVals As ArrayList, aChildVals As ArrayList
        Dim oTest As List(Of clsLoadedElement), oNewParentElemValue As clsLoadedElement
        Try
            aVals = New ArrayList(sLongPath.ToArray)
            If aVals.Count > 0 Then
                sCurrentKeyValue = aVals(0)
                If aVals.Count > 1 Then
                    oTest = GetChildElements(oParentElemValue)
                    For Each oElem In oTest
                        If oElem.bIsElement And oElem.sShortKey = aVals(0) Then
                            oNewParentElemValue = oElem
                            Exit For
                        End If
                    Next
                End If

                aChildVals = New ArrayList(sLongPath.ToArray)
                aChildVals.RemoveAt(0)
                If aChildVals.Count > 1 Then
                    sLongPath = Strings.Join(aChildVals.ToArray, sNodeNameDelimUsed)

                End If

            End If
        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try
        Return oReturn
    End Function



End Class

Public Class clsLoadedElement
    Implements IComparable(Of clsLoadedElement)

    Public sShortKey As String = ""
    Public iShortKeyCount As Integer = 0
    Public aParentNodeKeys As New ArrayList
    Public oValue As Object = Nothing
    Public bIsElement As Boolean = True
    Public XMLNamespaceURI As String = ""
    Public ParentNodeName As String = ""
    Protected Friend sNodeNameDelimUsed As String = sNodeNameDelimUsed

    Public aChildElements As New List(Of clsLoadedElement)
    Public ParentElement As clsLoadedElement = Nothing

    Public Sub New()

    End Sub

    Public Sub New(sParentKeys As String)
        aParentNodeKeys = New ArrayList(sParentKeys.Split(sNodeNameDelimUsed))

    End Sub

    Public Sub New(aParentKeys As ArrayList)
        aParentNodeKeys.AddRange(aParentKeys)

    End Sub

    Public Sub New(aParentKeys As ArrayList, sNewKey As String)
        aParentNodeKeys.AddRange(aParentKeys)
        sShortKey = sNewKey
    End Sub


    Public Property ParentKey As String
        Get
            Dim sReturn As String = ""
            sReturn = Join(aParentNodeKeys.ToArray, sNodeNameDelimUsed)
            Return sReturn

        End Get
        Set(value As String)
            Dim sCurr As String = ""
            Try
                sCurr = Join(aParentNodeKeys.ToArray, sNodeNameDelimUsed) & IIf(aParentNodeKeys.Count > 0, sNodeNameDelimUsed, "")
                If sCurr <> value Then aParentNodeKeys = New ArrayList(Split(value, "|"))
            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

            End Try

        End Set
    End Property


    Public Property LongKey As String
        Get
            Dim sReturn As String = ""
            sReturn = Join(aParentNodeKeys.ToArray, sNodeNameDelimUsed) & IIf(aParentNodeKeys.Count > 0, sNodeNameDelimUsed, "") & sShortKey
            If iShortKeyCount > 0 Then sReturn &= ":" & iShortKeyCount.ToString("D5")
            Return sReturn
        End Get
        Set(value As String)
            Dim sCurr As String = "", aVals As ArrayList
            Try
                sCurr = Join(aParentNodeKeys.ToArray, sNodeNameDelimUsed) & IIf(aParentNodeKeys.Count > 0, sNodeNameDelimUsed, "") & sShortKey
                If iShortKeyCount > 0 Then sCurr &= ":" & iShortKeyCount.ToString("D5")
                If sCurr <> value Then
                    aVals = New ArrayList(Split(value, sNodeNameDelimUsed))
                    If aVals.Count > 0 Then
                        sShortKey = aVals.Item(aVals.Count - 1)
                        aVals.RemoveAt(aVals.Count - 1)
                    End If
                    aParentNodeKeys = aVals
                End If
            Catch ex As Exception
                Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

            End Try
        End Set
    End Property


    Public Function CompareTo(other As clsLoadedElement) As Integer Implements IComparable(Of clsLoadedElement).CompareTo
        Dim iReturn As Integer = 0
        Dim iCnt As Integer, iMax As Integer

        Try
            iMax = Me.aParentNodeKeys.Count - 1
            If iMax < 0 Then
                iReturn = 1
                If other.aParentNodeKeys.Count < 1 Then
                    iReturn = Me.sShortKey.CompareTo(other.sShortKey)
                    If iReturn = 0 Then iReturn = Me.iShortKeyCount.CompareTo(other.iShortKeyCount)
                End If
            Else
                iCnt = -1
                Do While iReturn = 0 And iCnt < iMax
                    iCnt += 1
                    iReturn = 1
                    If other.aParentNodeKeys.Count >= iCnt Then
                        iReturn = Me.aParentNodeKeys.Item(iCnt).CompareTo(other.aParentNodeKeys(iCnt))
                        If iReturn = 0 Then iReturn = Me.iShortKeyCount.CompareTo(other.iShortKeyCount)
                    End If
                Loop
                If iReturn = 0 Then
                    iReturn = Me.sShortKey.CompareTo(other.sShortKey)
                    If other.aParentNodeKeys.Count > iMax + 1 Then iReturn = 1
                    If iReturn = 0 Then iReturn = Me.iShortKeyCount.CompareTo(other.iShortKeyCount)
                End If
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

        End Try

        Return iReturn
    End Function

End Class
