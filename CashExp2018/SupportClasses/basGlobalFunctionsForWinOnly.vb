﻿Imports System.Windows.Threading
Imports System.Threading
Imports System.Security.Permissions

Module basGlobalFunctionsForWinOnly

    Friend Sub ReportErrorGlobal(sErrorMessage As String)
        Dim oErrorItem As ErrorItem
        Try
            oErrorItem = New ErrorItem(sErrorMessage)
            ReportErrorGlobal(oErrorItem)
        Catch ex As Exception
            If gbHideGUI Then
                Console.Error.WriteLine(Replace(Replace(ex.ToString, "<br />", vbNewLine), "<br/>", vbNewLine).Replace(vbNewLine & vbNewLine, vbNewLine))
            Else
                MsgBox(ex.ToString, MsgBoxStyle.Critical)

            End If

        End Try
    End Sub


    Friend Sub ReportErrorGlobal(oErrItem As ErrorItem)
        Try
            If gsErrorLogPath <> "" Then
                Try : IO.File.AppendAllText(gsErrorLogPath, vbCrLf & Now.ToString("o") & vbCrLf) : Catch ex2 As Exception : End Try
                Try : IO.File.AppendAllText(gsErrorLogPath, oErrItem.ToString.Replace(vbCrLf & vbCrLf, vbCrLf)) : Catch ex2 As Exception : End Try
                Try : IO.File.AppendAllText(gsErrorLogPath, (oErrItem.oSourceEx.StackTrace.Replace(" at ", vbCrLf & "at ").Replace(" in ", vbCrLf & "in ") & vbCrLf).Replace(vbCrLf & vbCrLf, vbCrLf)) : Catch ex2 As Exception : End Try
                gsLastErrorLogFile = gsErrorLogPath
                Try
                    My.Settings.LastErrorLogFile = gsLastErrorLogFile
                    My.Settings.LastErrorDate = Now.ToString("o")
                    My.Settings.Save()
                Catch ex2 As Exception
                End Try
            End If

            If gbHideGUI Then
                'Console.Error.WriteLine(Replace(Replace(oErrItem.ToString, "<br />", vbCrLf), "<br/>", vbCrLf))
                ReportErrToCLI(Replace(Replace(oErrItem.ToString, "<br />", vbCrLf), "<br/>", vbCrLf).Replace(vbCrLf & vbCrLf, vbCrLf))
            Else
                MsgBox(Replace(Replace(oErrItem.ToUserString, "<br />", vbCrLf), "<br/>", vbCrLf).Replace(vbCrLf & vbCrLf, vbCrLf), MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal + vbMsgBoxSetForeground, "Error detected")
            End If

        Catch ex As Exception
            If gbHideGUI Then
                Console.Error.WriteLine(Replace(Replace(ex.ToString, "<br />", vbNewLine), "<br/>", vbNewLine).Replace(vbNewLine & vbNewLine, vbNewLine))
            Else
                MsgBox(ex.ToString, MsgBoxStyle.Critical)

            End If

        End Try
    End Sub



    Friend Function RecordErrorGlobal(exIn As Exception, Optional ExtraDets As ArrayList = Nothing) As ErrorItem
        Dim oErr As ErrorItem = Nothing
        Try

            oErr = glErrors.NewError(exIn)
            If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.aExtraDetail.AddRange(ExtraDets)
            gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")

            ReportErrorGlobal(oErr)

        Catch ex2 As Exception
            oErr = New ErrorItem(exIn)
        End Try
        Return oErr
    End Function

    Public Sub DoEvents()
        Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, New ThreadStart(Sub()
                                                                                             End Sub))
    End Sub

    <SecurityPermissionAttribute(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
    Public Sub DoEvents1()
        Dim frame As New DispatcherFrame()
        Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, New DispatcherOperationCallback(AddressOf ExitFrame), frame)
        Dispatcher.PushFrame(frame)
    End Sub

    Private Function ExitFrame(ByVal f As Object) As Object
        CType(f, DispatcherFrame).Continue = False

        Return Nothing
    End Function

End Module
