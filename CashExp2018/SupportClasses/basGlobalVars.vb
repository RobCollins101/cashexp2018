﻿Module basGlobalVars

    Public glErrors As New RunErrors
    Public gsErr As String = ""
    Public bLocalTest As Boolean = False
    Public gsTempPath As String = Logging.LogFileLocation.TempDirectory.ToString
    Public gsAppPath As String = My.Application.Info.DirectoryPath
    Public gsCurrentPath As String = My.Computer.FileSystem.CurrentDirectory

    Public gbResetReplacements As Boolean = True
    Public gsErrorLogPath As String = ""
    Public gsLastErrorLogFile As String = ""
    Public gsLogPath As String = ""
    Public gsLastLogFile As String = ""

    Public goCommandLineArgs As New Hashtable

    Public goValueSustitution As New clsValueSubstitutions

    Public gbHideGUI As Boolean = False
    Public gbProcessOnLoad As Boolean = False
    Public gbVerboseCLIFeedback As Boolean = False
    Public gbSilent As Boolean = False
    Public gbAllowExecutableInTemplates As Boolean = False

    Public giErrorReturnCode As Integer = 0

    Public gsParamListDivider As String = ""

    Public goMainSettings As New clsLoadedSettings
    Public goCurrLoadManager As clsLoadManager

End Module
