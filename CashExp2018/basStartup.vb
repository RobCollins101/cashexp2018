﻿Module basStartup

    Friend Sub MainStart()
        Dim bNoStart As Boolean = False
        Dim aFeedbackLines As New ArrayList

        bNoStart = SetupMainStart(aFeedbackLines)

        If bNoStart Then
            If aFeedbackLines.Count > 0 Then
                aFeedbackLines.Insert(0, "ERROR Program did not start correctly")
                aFeedbackLines.Add("Command lime parameters:[/T Type [/S Sourcepath ]] [/D Destpath]")
                aFeedbackLines.Add("/T for source type. Mandatory if using /S path to source file folder")
                aFeedbackLines.Add("/D Destination output cash file")
                aFeedbackLines.Add("/L Log of actions and errors file path")
                aFeedbackLines.Add("/T Switch for Time on messages.")
                'Console.Error.WriteLine(Join(aErrorLines.ToArray, vbNewLine))
                ReportErrToCLI(aFeedbackLines)
                aFeedbackLines.Clear()
            End If
            giErrorReturnCode = 1
        Else

            '=========================================================
            ' Do the perocessing
            ' Choose if CLI operation or GUI operation
            '=========================================================

            'If gbHideGUI Then
            '    'MainWindow.Hide()
            '    goCLIManageProcess = New clsManageSubstProcessesCLI
            '    goCLIManageProcess.ProjectDetails = goManageProcess.ProjectDetails

            '    goCLIManageProcess.MainProcessToDoProcessRecordSubst(goRecordValues)

            'Else

            If My.Application.StartupUri.OriginalString = "" Then

                gMainUIWindow = New wCashExpMainApp
                gMainUIWindow.Show()
                'End If
            End If

        End If

    End Sub

    ''' <summary>
    ''' Process to generate all necessary initialisation of process details
    ''' </summary>
    ''' <param name="aFeedbackLines"></param>
    ''' <returns></returns>

    Friend Function SetupMainStart(ByRef aFeedbackLines As ArrayList) As Boolean
        Dim bReturn As Boolean = False
        Try
            Dim sTemp As String, oTemp As Object ' , sKey As String
            Dim sTempSubst As clsValueSubstitutions

            gsErrorLogPath = GetMySettingString("ErrorLogFile")
            If gsErrorLogPath <> "" Then
                sTempSubst = New clsValueSubstitutions(gsErrorLogPath)
                gsErrorLogPath = Environment.ExpandEnvironmentVariables(sTempSubst.Result.Replace("-", "").Replace(":", ""))
                If gsErrorLogPath.IndexOf(IO.Path.DirectorySeparatorChar) < 0 Or gsErrorLogPath.IndexOf(IO.Path.DirectorySeparatorChar) > 3 Then gsErrorLogPath = My.Computer.FileSystem.CurrentDirectory &
                IIf(My.Computer.FileSystem.CurrentDirectory.Substring(My.Computer.FileSystem.CurrentDirectory.Length) <> IO.Path.DirectorySeparatorChar, IO.Path.DirectorySeparatorChar, "").ToString &
                gsErrorLogPath
                glErrors.sErrorLogFile = gsErrorLogPath
                glErrors.ResetErrors()
            End If
            gsLastErrorLogFile = GetMySettingString("LastErrorLogFile")


            'See https://docs.microsoft.com/en-us/dotnet/articles/visual-basic/developing-apps/customizing-extending-my/customizing-which-objects-are-available-in-my 

            goCommandLineArgs = GetCommandLineArgs()

            'If goCommandLineArgs.ContainsKey("V") Then gbVerboseCLIFeedback = True
            'If goCommandLineArgs.ContainsKey("Q") Then gbSilent = True : gbVerboseCLIFeedback = False
            If goCommandLineArgs.ContainsKey("T") Then gbTimeOnMessages = True

            If goCommandLineArgs.ContainsKey("?") Then

                aFeedbackLines.Add("Command lime parameters:[/T Type [/S Sourcepath ]] [/D Destpath]")
                aFeedbackLines.Add("/T for source type. Mandatory if using /S path to source file folder")
                aFeedbackLines.Add("/D Destination output cash file")
                aFeedbackLines.Add("/L Log of actions and errors file path")
                aFeedbackLines.Add("/T Switch for Time on messages.")
                ReportToCLI(aFeedbackLines, False)
                aFeedbackLines.Clear()
                bReturn = True
            Else

                If My.Settings.ProgramDataSettingsFile <> "" Then
                    sTemp = ValidPath(My.Settings.ProgramDataSettingsFile)
                    If IO.File.Exists(sTemp) Then
                        goMainSettings.sXMLFilePath = sTemp
                        goMainSettings.LoadToList()
                    End If
                End If

            End If

        Catch ex As Exception

        End Try

        Return bReturn
    End Function

End Module
